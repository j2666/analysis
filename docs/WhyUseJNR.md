# Why Use JNR-FFI?

Before using a library such as JNR-FFI or others that allow you to interact with native libraries you need to be fully
sure that this is something you actually need, as there are (sometimes hidden) costs to making such a decision.
JNR-FFIや、ネイティブライブラリと対話できる他の仕組みを使用する前に、
そのような決定を下すには（時々隠されている）コストがあるので、これがあなたが実際に必要なものであることが十分に検討する必要があります。

If you're looking for why to use JNR-FFI over some other similar
library [see the document that aims to answer that exact question.](ComparisonToSimilarProjects.md)

* [Why Go Native?](#why-go-native)
  * [Improving Performance](#improving-performance)
  * [Using Low Level APIs](#using-low-level-apis)
* [The Cost of Leaving the JVM](#the-cost-of-leaving-the-jvm)
  * [Compatibility](#compatibility)
  * [JVM Crashes](#jvm-crashes)
  * [Increased Complexity](#increased-complexity)
  * [Possible *Reduced* Performance](#possible-reduced-performance)
* [Endless Possibilities](#endless-possibilities)

## Why Go Native?

There are many reasons why one would consider interacting with the native world, they can often be summarized into:
それがネイティブの世界と対話することを検討する多くの理由があります、それらはしばしばまとめられています：
* [I need to improve the performance of my application](#improving-performance)
* [I need to use some low level API that is distributed as a native library such as OpenGL](#using-low-level-apis)

### Improving Performance

Going native does **not** guarantee that you will get improved performance, in fact it is often more beneficial to improve your *Java* code than to go native. This is simply because switching to and from the JVM incurs a significant overhead and disallows many JVM optimizations that have been developed over the last decades. 
Indeed, it is not uncommon to have pure Java code (especially well optimized) perform much better than Java with native code, even with well written JNI code.

ネイティブで実装することは、パフォーマンスが向上することを保証しません。
実際にはJavaのコードを改善する方が有効なことが、しばしばある
これは、JVMへの切り替えが大きなオーバーヘッドを招き、過去数十年にわたって開発された多くのJVMの最適化が行われなくなるためです。
確かに、よく書かれたJNIコードでも、純粋なJavaコード（特に最適化された）がネイティブコードを持つJavaよりもはるかに優れていることは珍しくありません。

**If your only reason for thinking about going native is for performance improvements, you may want to think carefully about that as the performance improvement is not a guarantee**, and the additional costs of adding native components to
your codebase is not insignificant, see [the below section for more.](#the-cost-of-leaving-the-jvm)

ネイティブになることを考えるのが唯一の理由がパフォーマンスの向上である場合は、パフォーマンスの向上は保証ではないので慎重に考えることをお勧めします。コードベースにネイティブコンポーネントを追加する追加のコストは無視できないものとなります。
詳しくは、the-cost-of-leaving-the-jvmの商を参照して下さい。

### Using Low Level APIs

This is the most common reason for going native, simply to use some low level API that the JDK does not provide (or at least not well enough). This is a good enough reason to go native, especially to provide or create bindings to a native library that you will be making extensive use of.
最も一般的な理由としては、いくつかのlow lvel API（JDKが提供していない、または不十分）を利用するといったケースです。
これは、特に、広範囲に使用するネイティブライブラリへのバインディングを提供または作成するために、ネイティブライブラリを利用するのに十分な理由です

Of course make sure that there do not already exist bindings for your library, for example there exist multiple excellent Java bindings to OpenGL alleviating the need for going native on your end as it has been handled for you.
もちろん、ライブラリのバインディングがまだ存在しないことを確認してください。たとえば、OpenGLへの優れたJavaバインディングが複数存在しています。
Also make sure that there isn't a library that provides the same or similar functionality already in pure Java.
また、純粋なJavaに既に同じ機能または類似の機能を提供するライブラリーがないことを確認してください。

For example, if all you need is file system manipulation then there exist many libraries that provide that functionality very well including the JDK's own file system APIs.
たとえば、必要なのはファイルシステムの操作である場合は、JDK独自のファイルシステムAPIを含めて、その機能を非常によく提供する多くのライブラリーが存在します。

If there isn't an already existing library for your needs then going to the native level is a good option especially if a native library already does what you intend and all you'll need to do is interact with it. 
あなたのニーズに既存のライブラリがすでに存在しない場合は、ネイティブレベルに進み、ネイティブライブラリがすでにあなたが意図するものをすでに行う場合は良い選択肢です。

This is th best reason to go the native level, however, be aware of [the costs that you will incur by leaving the JVM and interacting with the native world.](#the-cost-of-leaving-the-jvm)
これは、ネイティブレベルに行く最善の理由ですが、JVMを残してネイティブの世界と対話することによってあなたが根絶する費用を認識しています。

## The Cost of Leaving the JVM

Adding native components to your projects can have a significant and sometimes hidden costs that you may not be aware of until you're already heavily invested.
プロジェクトにネイティブコンポーネントを追加すると、あなたがすでに大きな投資されるまで、あなたが知っていないかもしれないことが重要であり、時々隠されたコストを持つことができます。
Knowing these costs early can help you be more informed about what decision to make and about how different your development experience may be.
これらの費用を早期に知っていることは、あなたがどのようにしてあなたの開発経験がどのように異なるかもしれないかについて何を与えるべきかについてもっと知らされるのを助けることができます。

### Compatibility

By far the greatest cost is the reduced compatibility of your program. The mantra "Write once, run anywhere" that applies to almost all Java programs (including yours without a native component) may no longer be true, and even if it remains true, you still have to be *platform-aware*, something most Java developers are not used to.
最大のコストはあなたのプログラムの互換性の低下です。
 "Write once, run anywhere"とは言えなくなります。仮に動くとしたとしても、プラットフォームを意識する必要があり、ほとんどのJava開発者はプラットフォームを意識して使うということに慣れていません。

There are three possibilities:
3つの可能性があります

* The library may not exist for all platforms; you now have less supported platforms（ライブラリはすべてのプラットフォームには存在しない可能性があり、あなたは今サポートされていないプラットフォームを持っています）

* The library may not behave identically on all platforms; your program may behave differently on different platforms（ライブラリはすべてのプラットフォームで同じように動作しない可能性があります。あなたのプログラムは異なるプラットフォームで異なる動作をするかもしれません）

* You may need to compile and distribute the library yourself for all your supported platforms; both of the above apply
  *AND* you need to compile and distribute the native library yourself, but you have full control(サポートされているすべてのプラットフォームに対して、ライブラリを自分でコンパイルして配布する必要があるかもしれません。上記のどちらも、ネイティブライブラリを自分でコンパイルして配布する必要がありますが、フルコントロールが必要です。)

In all cases you will always have *some* Java code that will do some platform checking to behave differently on
different platforms and this in itself is strange to many Java developers. すべての場合において、あなたは常に異なるプラットフォームで行動的に行動するようにいくつかのプラットフォームチェックをするいくつかのJavaコードを持っています、そしてそれ自体は多くのJava開発者にとって奇妙です。

Remember that a *"platform"* includes both Operating System *AND* CPU Architecture.
(Platformは、オペレーティングシステムとCPUアーキテクチャの両方が含まれていることを忘れないでください。)


### JVM Crashes

One of the more hidden costs of going native is that you are more likely to experience JVM crashes which can occur if your native library is used incorrectly and throws some error such as a segfault (SIGSEGV).
ネイティブの隠されたコストの1つは、ネイティブライブラリが誤って使用されている場合に発生する可能性があるJVMクラッシュを経験する可能性が高いということです。

The JVM will not know how to handle this, and you cannot catch these errors as you would with exceptions. Debugging them however, (at least for the source of error), is not too tricky as the JVM will provide extremely detailed crash logs. 
JVMはこれを処理する方法を知りません。ただし、（少なくともエラーの発生源の場合）は、JVMが非常に詳細なクラッシュログを提供するため、あまりにもトリッキーではありません。

A well tested and robust code-base will be well protected against these kinds of crashes but this cost is one that needs to be known beforehand as it is not often mentioned.
よくテストされた堅牢なコードベースは、これらの種類のクラッシュに対して十分に保護されますが、このコストはあまりにも述べられていないので事前に知られる必要があるものです。

### Increased Complexity

Becoming dependent on a native library can increase overall project complexity, especially when debugging, as you may need to be somewhat knowledgeable in C, and the native library in general to avoid the aforementioned crashes and have
generally correct behavior and performance. 
特に、Cではやや知識がある場合があるため、上記のクラッシュを避けるためには、特にデバッグ時には、全体的なプロジェクトの複雑さを高めることができます。
一般的に行動とパフォーマンスを訂正します。

Increased complexity is natural when depending on other APIs, but it is
important to note that debugging an error from a pure Java API will be much easier than that from a native library.
他のAPIに依存すると、複雑さの増加は自然ですが、それは
重要なJava APIからのエラーのデバッグは、ネイティブライブラリからのものよりもはるかに簡単になります。

### Possible *Reduced* Performance

Contrary to popular belief, it is possible to actually *lose* performance when using a native library over using a pure Java implementation. 
一般的な信念に反して、純粋なJava実装を使用してネイティブライブラリを使用するときに実際にパフォーマンスを失うことは可能です。

There is a significant overhead incurred when switching between the JVM and the native world that is unavoidable even if you use well optimized JNI code. 
よく最適化されたJNIコードを使用していても、避けられないJVMとネイティブの世界の間で切り替わるときに発生した大きなオーバーヘッドがあります。

In addition, many optimizations that are made for the JVM are completely circumvented by going to the native world as the VM is no longer in control of that code.
さらに、VMがもはやそのコードの制御がなくなったので、JVMに作られた多くの最適化は、ネイティブの世界に行くことによって完全に回避されます。

For all intents and purposes, JNR-FFI and other similar libraries will be performant enough for almost all applications and needs [(JNR-FFI being the fastest and comparable to hand-written JNI)](./ComparisonToSimilarProjects.md), even very realtime necessary ones, however, it will often be *more*
performant to stay on the JVM (ie, use a pure Java implementation) than to go native.
すべての目的や目的のために、JNR-FFIおよびその他の同様のライブラリは、ほとんどすべてのアプリケーションやニーズ（JNR-FFIが手書きのJNIに匹敵するもので、手書きのJNIに匹敵する）のために十分に実績があります。ネイティブに行くよりもJVM（すなわち、純粋なJava実装を使用）に滞在するのに慣れていることが多い。


 As mentioned [above](#improving-performance), **if your only reason for thinking about going native is for performance
improvements, you may want to think carefully about that as the performance improvement is not a guarantee.**

上述のように、ネイティブに貢献することを考えるのが唯一の理由がパフォーマンスの向上である場合は、パフォーマンスの向上は保証されないため、慎重に考えることをお勧めします。

## Endless Possibilities

Even though [the previous section](#the-cost-of-leaving-the-jvm) made it may seem like going native has more downsides
than upsides, the truth is, going native unleashes many possibilities and unshackles you from the limits of the JDK.
前のセクションであったとしても、ネイティブが上昇よりもダウンサイドが増えているように思われたとしても、真実は、多くの可能性を解き放ち、JDKの限界からあなたを覆い隠しています。

Think about this, if all Java libraries were built upon only the JDK, we would be much more limited in functionality in what we can do with Java, but because there are those who ventured outside the JDK and provided us with functionality
that the JDK was never willing to provide, we are wealthier in options and functionality than we were otherwise. 
これについて考えてください、すべてのJavaライブラリがJDKのみに構築された場合、Javaでできるものでは機能がはるかに限られていますが、JDKの外に換算し、機能を備えた人がいます。
JDKが提供しても構わないと思っていなかったことは、そうでなければ私たちよりも裕福です。

It's a high cost, high reward endeavor that if done well, will expand the Java Ecosystem further than ever imagined .
それはうまくいったら、想像以上にさらにJavaの生態系を拡大するのは、高いコストで高い報酬の取り組みです。

Game engines, GPU libraries, clustered computing, and many more were all made possible on Java by creating a Java API that connected to a native library and that would have otherwise never been possible by only using only the JDK or pure Java implementations.
ゲームエンジン、GPUライブラリ、クラスタ化コンピューティング、そして多くの多くは、ネイティブライブラリに接続されているJava APIを作成し、それ以外の場合はJDKまたは純粋なJava実装のみを使用しても不可能であったであろうことで、Java上ですべてが可能になりました。

If you have an idea and the only way to do it is by using a native library, JNR-FFI will make that endeavor much easier taking care of all the hassle for you and leaving you to focus on the important bits so that you can be the next innovator of the Java ecosystem.

あなたがネイティブライブラリを使用することによってあなたがアイデアとそれをする唯一の方法があるならば、JNR-FFIはあなたのためにすべての面倒の世話をすることをはるかに簡単にし、あなたができるように重要なビットに焦点を合わせることを努めるでしょう。Javaエコシステムの次のイノベーター。