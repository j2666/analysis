# Memory Management

## Pointers
A `Pointer` instance stores reference to a native memory address. As it can
represent various data like struct, arrays, java objects, etc. it needs to
be used with caution since any illegal access can cause segfaults and JVM crash.
The `Pointer` class provides methods to return different representations of the stored data, for example:
  - `getInt(long offset)`: Reads 32-bit int value at given offset
  - `getPointer(long offset, int size)`: Reads a pointer at given offset
  - `array()`: Returns an array if it backs this pointer
`Pointer`インスタンスはネイティブメモリアドレスへの参照を格納します。そのため、`Poineter`インスタンスは、構造体、配列、Javaオブジェクトなどのようなさまざまなデータを表します。
違法なアクセスでは、SEGFAULTSやJVM crashをおこす可能性があるため、注意して使用してください。  
`Pointer`クラスは、保存されたデータのさまざまな表現を返すためのメソッドを提供します。
例えば：
   -  `getInt（long offset）`：引数に指定したoffsetで、32ビット分をInt値として読み取る？
   -  `GetPointer（long Offset、Int size）`：引数に指定したoffsetで、指定されたサイズのポインタを読み取る？
   -  `array（）`：このポインタをバックした場合は配列を返す？

## Runtime
A `Runtime` instance for the loaded library can be obtained using the `Runtime.getRuntime()` method. It gives access to important services like `ObjectReferenceManager` and `MemoryManager`.
ロードされたライブラリの `Runtime`インスタンスは、` runtime.getRuntime（） `メソッドを使用して取得できます。それは `ObjectReferenceManager`と` MemoryManager`のような重要なサービスにアクセスできます。


### MemoryManager
As the name suggests, it provides various methods to allocate memory for use with native functions.
名前が示唆するように、ネイティブ関数で使用するためにメモリを割り当てるためのさまざまな方法を提供します。
  - `allocate`: Allocates Java memory
  - `allocateDirect`: Allocates native memory
  - `allocateTemporary`: Allocates transient native memory

The `Memory` class also provides utility methods to handle memory
allocation for common use-cases. It uses `MemoryManager` internally.
`memory`クラスは、共通のユースケースのメモリ割り当てを処理するためのユーティリティメソッドも提供します。それは内部的に `MemoryManager`を使います。

### ObjectReferenceManager
Any native memory associated with a Java object is released as soon as the object gets garbage-collected. 
An `ObjectReferenceManager` provides handy methods to keep objects strongly-referenced as long as its native memory is in use.
This can be helpful while working with function pointers, which are associated with lambda functions on the Java side.
Use `add` to register any object and use `remove` to dereference the registered object. 
Java オブジェクトがGCによって解放されると、そのオブジェクトに関連付けられているネイティブメモリは解放されます。
`ObjectReferenceManager`は、そのネイティブメモリが使用されている限り、オブジェクトを強く参照させるための便利なメソッドを提供します。
これは、Java側のLambda関数に関連付けられている関数ポインタを使用している間に役立ちます。`add`を使用してオブジェクトを登録し、登録されているオブジェクトを参照して` remove`を使用します。