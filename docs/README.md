# User Documentation

Here is where you will find documents that explain aspects about JNR-FFI such as how to quickly get started or whether
to even use JNR-FFI.

これは、すぐに開始する方法など、JNR-FFIに関する側面を説明する文書を見つける場所です。
JNR-FFIを使用する。

* [Getting Started](./GettingStarted.md)
* [Type Mappings](./TypeMappings.md)
* [Memory Management](./MemoryManagement.md)
* [Why Use JNR-FFI](./WhyUseJNR.md)
* [Comparison to Similar Projects](./ComparisonToSimilarProjects.md)
* [Projects using JNR-FFI](./ProjectsUsingJNR.md)
* [Squeezing Performance](./SqueezingPerformance.md)
* [Frequently Asked Questions](./FAQ.md)