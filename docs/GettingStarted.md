# Getting Started

## Hello World

Hello World is as easy as:

```java
import jnr.ffi.LibraryLoader;

public class HelloWorld {
    public interface LibC { // A representation of libC in Java
        int puts(String s); // mapping of the puts function, in C `int puts(const char *s);`
    }

    public static void main(String[] args) {
        LibC libc = LibraryLoader.create(LibC.class).load("c"); // load the "c" library into the libc variable

        libc.puts("Hello World!"); // prints "Hello World!" to console
    }
}
```

## Step-by-Step

The mapping process is simple even for larger, more complex use cases.

For the example below we'll use some of the C functions regarding environment variables.

マッピングプロセスは、より大きく複雑な使用例でさえも簡単です。

以下の例では、環境変数に関するC関数のいくつかを使用します。

### 1. Create an interface which maps the desired functions of the native library
ネイティブライブラリの目的の機能をマッピングするインターフェイスを作成する
We want to use these C functions in Java:

```c
int setenv(const char *name, const char *value, int overwrite);

int unsetenv(const char *name);

char *getenv(const char *name);

int clearenv(void);
```

We do so by creating an interface that represents the native library and add the functions with the type mappings from C
to Java, all mappings can be seen in the [Type Mappings document](TypeMappings.md).
私たちは、ネイティブライブラリを表すインターフェースを作成し、その関数をCからのタイプマッピングで追加することによってそうします。
Javaには、[タイプマッピング文書]（typemappings.md）にすべてのマッピングが表示されます。

This results in:

```java
    public interface LibC {
        int setenv(String name, String value, boolean overwrite); // overwrite can be int but boolean makes more sense

        int unsetenv(String name);

        String getenv(String name);

        int clearenv();
    }
```

We can add annotations for improved performance to tell JNR-FFI about how we will use parameters so it knows how to
optimize conversions. You can read more about this in the [Squeezing Performance document](SqueezingPerformance.md).
This is not necessary (as is stated in the [Squeezing Performance document](SqueezingPerformance.md)) but demonstrates
the flexibility and control JNR-FFI provides you if you have use for it.

パラメータを使用する方法については、パフォーマンスを向上させるためのパフォーマンスを向上させるための注釈を追加できます。
変換を最適化します。[Squeezing Performance Document]（SqueezingPerformancatance.md）でこれについてもっと読むことができます。
これは必須ではありません（[Squeezing Performance Document]（SqueezingPerformance.md））が実証されています。
柔軟性とコントロールJNR-FFIは、使用している場合はあなたが提供します。




```java
public interface LibC {
    int setenv(@In String name, @In String value, @In boolean overwrite);

    int unsetenv(@In String name);

    String getenv(@In String name);

    int clearenv();
}
```

### 2. Load the native library

Next we need to tell JNR-FFI how to load our library:

```java
import java.util.HashMap;

import jnr.ffi.LibraryLoader;

public class Example {
    public static void main(String[] args) {
        // Add library options to customize library behavior
        Map<LibraryOption, Object> libraryOptions = new HashMap<>();
        libraryOptions.put(LibraryOption.LoadNow, true); // load immediately instead of lazily (ie on first use)
        libraryOptions.put(LibraryOption.IgnoreError, true); // calls shouldn't save last errno after call
        String libName = Platform.getNativePlatform().getStandardCLibraryName(); // platform specific name for libC

        LibC libc = LibraryLoader.loadLibrary(
                LibC.class,
                libraryOptions,
                libName
        );
    }
}
```

Note how in the above example we used a more complex way to load the library than before.
This way we can customize the behavior of the library in both how its loaded and how JNR-FFI calls it.
注記上記の例では、以前よりもライブラリをロードするためのより複雑な方法を使用しました。
このようにして、ロードされた方法とJNR-FFIの呼び出しの両方でライブラリの動作をカスタマイズできます。

JNR-FFI is very flexible and gives you a lot of options and control over how the native library is called, see the
[`LibraryLoader`](../src/main/java/jnr/ffi/LibraryLoader.java) class for more.
JNR-FFIは非常に柔軟で、あなたに多くのオプションを与え、ネイティブライブラリの呼び出された方法を制御します。
詳細は [`LibraryLoader`](../src/main/java/jnr/ffi/LibraryLoader.java) class を見てください

Also note the `libName` variable instead of `"c"`, this is because some platforms (namely Windows) use a different name
for the C standard library. The [`Platform`](../src/main/java/jnr/ffi/Platform.java) class is aware of platform specific
information such as this.

また、 `" c "の代わりに` libname`変数に注意してください。これは、プラットフォーム（すなわちWindows）によっては、
C標準ライブラリと異なる名前を使用するためです。
[`platform`]（../ SRC / Main / Java / JNR / FFI / Platform.java）クラスが、それらの対応を解決します。

### 3. Call the native library functions

Now we can use the native library as if it is a Java API, JNR-FFI will handle the calling and converting for us.
これで、Java APIのようにネイティブライブラリを使用できます.JNR-FFIは、呼び出しを処理し、私たちのための変換を処理します。

```java
public class Example {
    public static void main(String[] args) {
        Map<LibraryOption, Object> libraryOptions = new HashMap<>();
        libraryOptions.put(LibraryOption.LoadNow, true);
        libraryOptions.put(LibraryOption.IgnoreError, true);
        String libName = Platform.getNativePlatform().getStandardCLibraryName();

        LibC libc = LibraryLoader.loadLibrary(
                LibC.class,
                libraryOptions,
                libName
        );

        final String pwdKey = "PWD"; // key for working directory
        final String shellKey = "SHELL"; // key for system shell (bash, zsh etc)

        String pwd = libc.getenv(pwdKey);
        System.out.println(pwd); // prints current directory

        libc.setenv(pwdKey, "/", true); // set PWD to /
        System.out.println(libc.getenv(pwdKey)); // prints /

        libc.unsetenv(pwdKey); // unset PWD
        System.out.println(libc.getenv(pwdKey)); // prints null (it is null not the String "null")

        System.out.println(libc.getenv(shellKey)); // prints system shell, /bin/bash on most Unixes
        libc.clearenv(); // clear all environment variables
        System.out.println(libc.getenv(shellKey)); // prints null (it is null not the String "null")
        System.out.println(libc.getenv("_")); // even the special "_" environment variable is now null
    }
}
```

If you are interested in using POSIX functions in Java, check out our other
library [JNR-POSIX](https://github.com/jnr/jnr-posix), it uses JNR-FFI to call POSIX functions and is a good place to
see production code that uses JNR-FFI.
JavaでPOSIX関数を使用することに興味がある場合、 [JNR-POSIX](https://github.com/jnr/jnr-posix)を見てください。
JNR-FFIを使用してPOSIX関数を呼び出すサンプルがのっています。
