# Comparison to Similar Projects

JNR-FFI is by no means the first or only library or tool that aims to connect Java to the native world.
JNR-FFIはJavaをネイティブの世界に接続することを目的とした最初、または唯一のライブラリ、ツールというわけではありません。
 There exist many projects similar to JNR-FFI which approach this problem in various ways.
 さまざまな方法でこの問題に取り組んでいるJNR-FFIと同様のプロジェクトが多数あります。  
 
Below are *some* of the popular ones and how they compare to JNR-FFI.
以下は人気のあるもののいくつか、そして彼らがJNR-FFIと比較する方法です。
Of course, we try our best to be as objective as possible, but of course there will be some level of bias given the circumstances.
もちろん、私たちはできるだけ客観的になるために最善を尽くしますが、状況を考えると、ある程度のバイアスがあるでしょう。

Before choosing which library to use to connect to the native world, make sure that going native is the right choice for you and that you are aware of the costs that doing so incurs by reading the document [Why Use JNR.](./WhyUseJNR.md)
ネイティブの世界に接続するためにどのライブラリを使用するかを選択する前に、ネイティブがあなたに適した選択であること、そしてあなたがその文書を読むことによって起こるコストを知っていることを確認してください。

## JNA (Java Native Access)

[JNA (Java Native Access)](https://github.com/java-native-access/jna) is the closest library to JNR-FFI. 
JNAはJNR-FFIへの最も近いライブラリです。

JNA has the same core goal, easy access to native libraries. 
JNAは同じ主要な目標を持っており、ネイティブライブラリへの簡単なアクセスができるものです。
JNA also has a similar approach to doing this by making users write mappings and using the calls to the mapped functions as requests to call to the native library functions. 
JNAは似たアプローチを持っています。 具体的には、ユーザーがマッピングを作成し、マップされた関数への呼び出すことで、ネイティブライブラリ関数に呼び出すというアプローチです。

JNA even has quite similar APIs.
JNAは、まったく同様のAPIを持ってさえいます。
Despite this though, there are some important differences that you should take into consideration before choosing which one to use.
それにもかかわらず、重要な違いがあるので、どれを使用するのかを選択する前に考慮する必要があります。

### Why use JNR-FFI
JNR-FFI has a much more complete and modern API.
JNR-FFIには、はるかに完全で現代的なAPIがあります。
JNR-FFI encourages you to use type alias annotations as much as possible to help keep width and size of parameters correct on different platforms.
JNR-FFIはパラメータの幅とサイズを異なるプラットフォームで正しく保つのに役立てるため、できるだけ、タイプエイリアスアノテーションを使用することを勧めます 
 
On JNA you need to create a special `SizeT` class type for your `size_t` types but with JNR-FFI all you need is an annotation `@size_t`. 
JNAでは、 `size_tの種類のために特別な` SIZET`クラスタイプを作成する必要がありますが、JNR-FFIで必要なのは注釈 `@ size_t`だけです。

JNA requires you to create inner classes to support a by-reference parameter whereas in JNR-FFI you can use any of the `ByReference` classes such as `DoubleByReference` or `PointerByReference`.
JNAは、参照パラメータをサポートするための内部クラスを作成する必要があります。それに対して、JNR-FFIでは `DoubleByReference`や` PointerByReference`などの `By Reference`クラスを使うことができます

JNR-FFI has significantly superior performance, nearing that of hand-written JNI in some cases, even in comparison to JNA's *"direct mapping"*.
JNR-FFIのパフォーマンスは大幅に優れています。手書きのJNIのそれに近く、JNAの「直接マッピング」と比較しても。

[Alexander Zakusylo has an excellent benchmark showing this](https://github.com/zakgof/java-native-benchmark). 
Of course, performance is only a small part of the equation, and the difference is likely negligible for many use cases, however, if performance is a top priority for you, JNR-FFI is the superior choice.
もちろん、パフォーマンスは式のわずかな部分にすぎず、違いは多くのユースケースには無視できますが、パフォーマンスが上位の場合はJNR-FFIが優れた選択です。


### Why use JNA
JNA is much more mature than JNR-FFI. 
JNAはJNR-FFIよりもはるかに成熟しています。
With more commits, more contributors, more users, and used in projects by large
companies, JNA has a larger and more vibrant community.
より多くのコミット、より多くの貢献者、より多くのユーザー、そして大きいプロジェクトで使用される企業、JNAはより大きくてもっと活気のあるコミュニティを持っています。

If you have a question with JNA, chances are, it's already been asked and answered. 
あなたがJNAに質問がある場合、すでに尋ねられて回答されている可能性がある。

JNR-FFI in comparison is younger and smaller but still used by many including
JRuby, [see who uses JNR-FFI here.](./ProjectsUsingJNR.md)
JNR-FFIは、若く、小さいが、JRubyで多く含まれている。

Somewhat related to its better maturity, JNA has better overall documentation too.
JNAはより成熟されているため、全般的にドキュメントも成熟しています。

 Again, if you have a question with JNA, there's a good chance it's answered by the documentation or at least clarified by it. 
繰り返しますが、JNAに質問がある場合は、ドキュメントによって答えられるか、少なくともそれによって明確化されている可能性があります。
 
 This also makes debugging with JNA generally easier because you can skim through the code with the well written JavaDoc comments to guide you.
 JNAを使う場合のデバッグは一般的にはより簡単です。よく書かれたJavaDocのコメントにより、コードをざっと見ることができる。

This is something JNR-FFI is aiming to improve on (these documents are a start and testament to that resolve), but JNA is still better by some margin in this regard.
これは、JNR-FFIがオンに向上させることを目的としているものです（これらの文書はその解決策の開始と称賛です）。しかしJNAは、この点に関していくつかのマージンによって依然として良好である。


## Project Panama

Many years ago the OpenJDK team announced [Project Panama](https://openjdk.java.net/projects/panama/), an official addition to the JDK itself to provide better interaction with native libraries. 
何年も前にOpenJDKチームがプロジェクトパナマ、JDK自体へのオフィシャル追加で、ネイティブライブラリとの交流を発表しました。

This has been many years in the making and seen many delays and hiccups but may prove to be a great solution to the problem that can be built into the JDK itself. 
これは何年もの間、多くの遅れやハシュップを見たが、JDK自体に組み込むことができる問題に対する素晴らしい解決策になることが証明されるかもしれません。

However, Project Panama currently (as of May 2021) has some serious shortcomings in comparison to its competitors including JNR-FFI.
しかし、プロジェクトパナマ現在（2021年5月現在）JNR-FFIを含む競合他社と比較して、いくつかの重大な欠点があります。

### Why use JNR-FFI
Unlike Project Panama, JNR-FFI is available today to use by all and is in fact used in production code [on many projects including JRuby](./ProjectsUsingJNR.md).
Project Panamaとは異なり、JNR-FFIは今日使用可能であり、実際にはJRubyを含む多くのプロジェクトで実用化コードで使用されています。

While JNA may be more mature than JNR-FFI, both are significantly more mature and tested than Project Panama which is not yet officially released and has seen delays before.
JNAはJNR-FFIよりも成熟している可能性がありますが、両方ともプロジェクトパナマよりも大幅に成熟してテストしています。Project Panamaは、まだ正式に解放されておらず、遅延しいています。

Even if Project Panama was released today, it would likely require the latest JDK to be usable on, leaving much of the Java user-base out of its functionality. 
プロジェクトパナマが今日リリースされたとしても、最新のJDKが使用可能になることが必要になるでしょう。
JNR-FFI requires only requires a JDK level of 8 or higher meaning even projects
still stuck behind on older JDKs can enjoy the benefits JNR-FFI brings.
JNR-FFIでは、プロジェクトでさえ8つ以上の意味のJDKレベルのみが必要です
それでも古いJDKの後ろに立ち往生していますJNR-FFIがもたらす利点をお楽しみいただけます。

Another thing to note is that JNR-FFI is constantly being updated and improved and gaining access to those improvements is as easy as changing a couple of lines in your `build.gradle` or `pom.xml`. 
注意するもう1つのことは、JNR-FFIが絶えず更新され、改善されていますが、`build.gradle` or `pom.xml`によって簡単に取り込むことができます。

Updating your JDK (which Project Panama will be built into) is not as straightforward and updates to Project Panama, or even the JDK for that matter, may be less frequent or impressive than those from JNR-FFI.
Project Panamaが組み込まれているJDKを更新することは簡単ではなく、プロジェクトパナマの更新するためのJDKのupdateは、JNR-FFIと比較して、頻度が少ない。

### Why use Project Panama
Theoretically Project Panama should have significant improvements over its competition by the fact it will likely be built into the platform.
理論的にプロジェクトのパナマは、それがプラットフォームに組み込まれる可能性が高いという事実によってその競争に対して大きな改善を遂げるべきです。

This means that Project Panama will probably have a close connection to the JDK and even JVMs in general, improving usability and performance since they can change the JDK to fit in with Project Panama. 
つまり、プロジェクトパナマはおそらくJDKやJVMにも密接に接続されており、JDKがプロジェクトパナマと収まるようにJDKを変更できるため、ユーザビリティとパフォーマンスを向上させることができます。

If this is indeed the case, then the reasons to use anything *but* Project Panama would be very slim.
これが確かにそうである場合は、プロジェクトパナマを使用する理由は非常にスリムになるでしょう。

Similar to the above, since Project Panama is built by those who develop the JDK itself, they're probably some of the best Java developers in the world with the greatest knowledge of the Java platform as a whole. 
プロジェクトパナマはJDK自体を発展させる人々によって構築されているので、Javaプラットフォーム全体の最大の知識を持つ世界で最も良いJava開発者の一部です。


Thus, their creation (Project Panama) is likely to be of a very high quality and likely more well engineered and developed than others.
したがって、彼らの創造（プロジェクトパナマ）は非常に高品質であり、よりよく設計され、他の人よりも開発される可能性が高い。

 This is of course only an assumption, but one that is at least somewhat sound, especially given the previous point about Project Panama's deep integration with the platform as a whole as well.
これはもちろん仮定だけですが、特にプロジェクトパナマとプラットフォーム全体としてのプロジェクトパナマの深い統合についての前のポイントを考えると、少なくとも幾分音がしているものです。



## JNI (Java Native Interface)

JNI (Java Native Interface) is the oldest of all and is the original way of connecting Java to the native world. JNI was
designed early in Java's development and growth and was for a long time, the only way to connect Java to the native
world. However, there are some significant and important issues to be aware of with JNI, which themselves lead to the
popularity of all the previous tools.

### Why use JNR-FFI

Using JNR-FFI means going purely Java, you don't need to write tedious error-prone JNI C code. This is especially true
if you are not the developer of the native library you wish to call and are just using it as an API, something that is a
very common use case. In such a use case with JNI, you would still need to write JNI code in C and have to compile it
and ensure its correctness. By using JNR-FFI you are essentially cutting your possible code-base size significantly (in
comparison to a JNI based approach) and using only Java, something many Java developers would (obviously) find very
appealing.

JNR-FFI is generally more cross-platform compatible, at least given your library exists on all platforms. Of course this
assumes cross-platform availability of your library but even if that is only partially true, it is still better than JNI
which requires you to *compile* the C code for all platforms you intend to support, essentially completely stripping
away the cross-platform ease of Java development completely. With JNR-FFI, you can at least never have to worry about
compiling C libraries for different platforms.

JNR-FFI is very fast, nearly as fast as JNI in some cases and with none of the pain points. Writing *good* JNI code that
is actually performant and safe is more difficult than it looks. JNR-FFI alleviates all of that and is effectively  
just as fast and in some cases comparable, providing you with all the benefits and none of the problems.

### Why use JNI

The arguments for using JNI over JNR-FFI or any other similar alternative listed above are quite weak. In almost every
case, you will want to use something that *isn't* JNI, but there are a couple of arguments for JNI nonetheless.

For size conscious applications, a JNI implementation will likely provide the smallest size, especially if the supported
platforms are few. This is a rare occurrence but Java is known to run on even small smart cards or other platforms where
size requirements can be an active concern. Depending only on a minimal JDK and some custom JNI libraries is *much*
less size consuming than any non-JNI based alternative.

JNI is the most performant way of accessing native libraries with Java. This is true, but only gives part of the
picture. Firstly, JNR-FFI is extremely performant, in some cases coming comparable to hand-written JNI. Secondly, and
more importantly, writing **performant** *and* **safe** JNI code is difficult and very error-prone. The development cost
necessary to gain such a small improvement in performance is for most and seen by most as a bad trade-off. Nevertheless,
if the absolute performance limits of Java are what is necessary for you at *any* cost, then JNI is more suitable for
you.