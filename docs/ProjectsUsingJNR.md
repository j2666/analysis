# Projects Using JNR-FFI

## Our Projects

These are projects developed and maintained by (generally) the same team that develops and maintains JNR-FFI.

### JRuby

By far the largest, most substantial and most important project using JNR-FFI is JRuby, a JVM implementation of the Ruby programming language.
JNR-FFIを使用した最大の、最も実質的で最も重要なプロジェクトは、JRuby、Rubyプログラミング言語のJVM実装です。

 In fact JNR-FFI started out as a library for JRuby to solve Ruby's need for native system calls and provide the same level of intractability with the native world as other Ruby implementations did. 
実際、JNR-FFIは、JRubyのライブラリとして始まりました。これは、Rubyのシステムコールに対するRubyの必要性を解決し、他のRubyの実装を行ったように、ネイティブの世界と同じレベルの侵害性を提供しました。


 JRuby is JNR-FFI's largest *"client"* and indeed the lead maintainers of JNR-FFI and the JNR project in general are also the lead developers of JRuby currently employed by Red Hat.
  JRubyはJNR-FFIの最大の*「クライアント」*であり、実際にはJNR-FFIのリードメンテナと一般的なJNRプロジェクトもRed Hatが現在雇用されているJRubyのリード開発者です。 

  This is all to say that we are *"dogfooding"* JNR-FFI in our own major large-scale project JRuby.
これは、私たちが自分の主要な大規模プロジェクトJRubyで、JNR-FFIを実際に試しているということです。

If you had doubts about whether this library or even the JNR project in general is professional or will survive the
foreseeable future, JRuby's deep dependence on JNR-FFI (and the JNR project) should eliminate those doubts.

### JNR Projects

The JNR Project has multiple libraries that use JNR-FFI to call to native libraries such as JNR-POSIX, JNR-Process and JNR-ENXIO. 
JNRプロジェクトには、JNR-FFIを使用してJNR-POSIX、JNR-PROCESS、およびJNR-ENXIOなどのネイティブライブラリを呼び出すライブラリが複数あります。

Many of these themselves used in JRuby to help increase ease (and performance) of native library calls for JRuby.
これら自身の多くはJRubyで使用され、ネイティブライブラリーのjruby呼び出しの容易さ（そしてパフォーマンス）を増やすのに役立ちます。


## Community Projects

These are projects developed by the community.

### [JRtMidi](https://github.com/basshelal/JRtMidi)

Java bindings to [RtMidi](https://github.com/thestk/rtmidi) written in Kotlin.

--------------------------------------------------

If you're using JNR-FFI and want to add your project here, make a pull request!