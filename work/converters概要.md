# jnr.ffi.provider.convertersのクラス一覧

## ToNativeConverterのアノテーション設定有無

@ToNativeConverter.NoContext,@ToNativeConverter.Cacheableの設定有無を以下にまとめる

|Class|概要|NoContext|Cacheable|
|:--|:----------|:--:|:--:|
|BoxedBooleanArrayParameterConverter|Converts a Boolean[] array to a primitive boolean[] array parameter|〇|〇|
|BoxedByteArrayParameterConverter|Converts a Byte[] array to a byte[] array parameter|〇|〇|
|BoxedDoubleArrayParameterConverter|Converts a Double[] array to a double[] array parameter|〇|〇|
|BoxedFloatArrayParameterConverter|Converts a Float[] array to a float[] array parameter|〇|〇|
|BoxedIntegerArrayParameterConverter|Converts a Integer[] array to a primitive int[] array parameter|〇|〇|
|BoxedLong32ArrayParameterConverter|Converts a Long[] array to a primitive int[] array parameter|〇|〇|
|BoxedLong64ArrayParameterConverter|Converts a Long[] array to a primitive 64bit long[] array parameter|〇|〇|
|BoxedShortArrayParameterConverter|Converts a Short[] array to a primitive short[] array parameter|〇|〇|
|ByReferenceParameterConverter|説明なし|-|〇|
|CharSequenceArrayParameterConverter|Converts a CharSequence[] array to a Pointer parameter|〇|〇|
|CharSequenceParameterConverter|Converts a CharSequence (e.g.|〇|〇|
|EnumConverter|説明なし|〇|〇|
|EnumSetConverter|説明なし|-|〇|
|Long32ArrayParameterConverter|Converts a long[] array to a primitive int[] array parameter|〇|〇|
|NativeLong32ArrayParameterConverter|Converts a NativeLong[] array to a primitive int[] array parameter|〇|〇|
|NativeLong64ArrayParameterConverter|Converts a NativeLong[] array to a primitive long[] array parameter|〇|〇|
|NativeLongConverter|Parameter and return type support for the old NativeLong type|〇|〇|
|Pointer32ArrayParameterConverter|Converts a Pointer[] array to a int[] array parameter|〇|〇|
|Pointer64ArrayParameterConverter|Converts a Pointer[] array to a long[] array parameter|〇|〇|
|StringBufferParameterConverter|説明なし|〇|〇|
|StringBuilderParameterConverter|説明なし|〇|〇|
|StringResultConverter|Converts a native pointer result into a java String|-|-|
|StructArrayParameterConverter|Converts a Pointer[] array to a long[] array parameter|〇|〇|
|StructByReferenceFromNativeConverter|Converts a native pointer result into a Struct|-|-|
|StructByReferenceToNativeConverter|説明なし|〇|〇|

## FromNativeConverterのアノテーション設定有無

@FromNativeConverter.NoContext,@FromNativeConverter.Cacheableの設定有無を以下にまとめる

|Class|概要|NoContext|Cacheable|
|:--|:----------|:--:|:--:|
|BoxedBooleanArrayParameterConverter|Converts a Boolean[] array to a primitive boolean[] array parameter|-|-|
|BoxedByteArrayParameterConverter|Converts a Byte[] array to a byte[] array parameter|-|-|
|BoxedDoubleArrayParameterConverter|Converts a Double[] array to a double[] array parameter|-|-|
|BoxedFloatArrayParameterConverter|Converts a Float[] array to a float[] array parameter|-|-|
|BoxedIntegerArrayParameterConverter|Converts a Integer[] array to a primitive int[] array parameter|-|-|
|BoxedLong32ArrayParameterConverter|Converts a Long[] array to a primitive int[] array parameter|-|-|
|BoxedLong64ArrayParameterConverter|Converts a Long[] array to a primitive 64bit long[] array parameter|-|-|
|BoxedShortArrayParameterConverter|Converts a Short[] array to a primitive short[] array parameter|-|-|
|ByReferenceParameterConverter|説明なし|-|-|
|CharSequenceArrayParameterConverter|Converts a CharSequence[] array to a Pointer parameter|-|-|
|CharSequenceParameterConverter|Converts a CharSequence (e.g.|-|-|
|EnumConverter|説明なし|〇|〇|
|EnumSetConverter|説明なし|-|〇|
|Long32ArrayParameterConverter|Converts a long[] array to a primitive int[] array parameter|-|-|
|NativeLong32ArrayParameterConverter|Converts a NativeLong[] array to a primitive int[] array parameter|-|-|
|NativeLong64ArrayParameterConverter|Converts a NativeLong[] array to a primitive long[] array parameter|-|-|
|NativeLongConverter|Parameter and return type support for the old NativeLong type|〇|〇|
|Pointer32ArrayParameterConverter|Converts a Pointer[] array to a int[] array parameter|-|-|
|Pointer64ArrayParameterConverter|Converts a Pointer[] array to a long[] array parameter|-|-|
|StringBufferParameterConverter|説明なし|-|-|
|StringBuilderParameterConverter|説明なし|-|-|
|StringResultConverter|Converts a native pointer result into a java String|〇|〇|
|StructArrayParameterConverter|Converts a Pointer[] array to a long[] array parameter|-|-|
|StructByReferenceFromNativeConverter|Converts a native pointer result into a Struct|-|-|
|StructByReferenceToNativeConverter|説明なし|-|-|