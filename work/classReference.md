
全部入り（没）
コピーペーストのため残しておく

```plantuml
@startuml
package java.lang {
    class ClassLoader
}
package jnr.ffi.provider.jffi {
    class AsmBuilder
    class AsmClassLoader extends ClassLoader
    class NativeLibrary
    class NativeLibraryLoader extends jnr.ffi.LibraryLoader

    interface MethodGenerator
    class X86MethodGenerator implements MethodGenerator
    class BaseMethodGenerator implements MethodGenerator
    
    class SkinnyMethodAdapter extends org.objectweb.asm.MethodVisitor implements org.objectweb.asm.Opcodes
    class X86_64StubCompiler extends AbstractX86StubCompiler
    class AbstractX86StubCompiler extends StubCompiler

    class AsmLibraryLoader extends LibraryLoader
    abstract class LibraryLoader
    
    class DefaultInvokerFactory
}
package jnr.ffi.provider{
    class InterfaceScanner
    class NativeFunction
}
package jnr.ffi {
    class jnr.ffi.LibraryLoader
    
}
package ユーザ定義 {
    class interfaceClass
}
package org.objectweb.asm{
    class org.objectweb.asm.ClassWriter
    class org.objectweb.asm.MethodVisitor 
    interface org.objectweb.asm.Opcodes
}

AsmBuilder -- AsmLibraryLoader


AsmLibraryLoader -up- AsmClassLoader
AsmLibraryLoader -- MethodGenerator : バイトコードの生成
AsmLibraryLoader -up- org.objectweb.asm.ClassWriter

AsmLibraryLoader -- StubCompiler
AsmLibraryLoader -- DefaultInvokerFactory
AsmLibraryLoader -- InterfaceScanner
AsmLibraryLoader -- SkinnyMethodAdapter
MethodGenerator -left- AsmBuilder
MethodGenerator -- StubCompiler
@enduml
```