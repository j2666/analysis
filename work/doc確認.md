

# docs
## README.md
 ドキュメントの読む順番がかかれている。
 以下、READMEの順番で見直し

## GettingStarted.md 

### サンプルによる説明
puts等をjnr経由で呼び出すサンプル（自分で作成したライブラリを呼び出す例はない。）
呼び出したい関数のひな形をInterfaceとして定義して、決まったルールで実体化するとその関数を呼びだせる。

### 外部ライブラリを読み込む際のオプションの補足
- 外部ライブラリを読み込む際のオプションの補足あり
- オプション [`LibraryLoader`](../src/main/java/jnr/ffi/LibraryLoader.java) 
- プラットフォームによって関数名のルールが違うので、その場合のオプション指定方法[`platform`]（../ SRC / Main / Java / JNR / FFI / Platform.java）

### POSIX関数を使用方法
 [JNR-POSIX](https://github.com/jnr/jnr-posix)というプロジェクトで作成されている

## TypeMappings.md
javaとCの型がどの型に対応するかの説明
eunum,call back関数も使える
unsingend long(64bit)はサポートされない

## MemoryManagement.md
Pointerクラスなどの説明があったが、利用例がなかったので、別途試す必要あり

## WhyUseJNR.md
JNRを使う目的が何か？を考えるための注意点が記載されている。
パフォーマンス自体はJavaで最適化された方がよくなることもあるので
それ以外にJavaで利用できない低レベルAPIの利用したいとか
そういった目的なのか？とかをちゃんと考えたほうがいい。
移植性が悪くなるとか、複雑になるといったデメリットをちゃんと考慮すること

## ComparisonToSimilarProjects.md
JNA、Project Panama、JNIとの比較が書かれている

## ProjectsUsingJNR.md
JRuby、JNR-POSIX、JNR-PROCESS、JNR-ENXIOなどがある。
JNR-POSIX、JNR-PROCESS、JNR-ENXIOはJRubyで使われている。


## SqueezingPerformance.md
性能をより出すためのtipsが書かれている

## FAQ.md
質問やissue 等の問い合わせ先が書かれている。
pull reqを送る前にissueとかに書いてね

