
# 作業メモ

## 確認対象

```
├── docs                                     9ファイル
├── libtest                                 12ファイル
│   └── struct                               5ファイル
├── src
│   ├── main
│   │   └── java
│   │       └── jnr
│   │           └── ffi                      19ファイル
│   │               ├── annotations          17ファイル
│   │               ├── byref                13ファイル
│   │               ├── mapper               28ファイル
│   │               ├── provider             32ファイル
│   │               │   ├── converters       26ファイル
│   │               │   └── jffi             73ファイル
│   │               │       └── platform: アーキテクチャ,OS でsubdirectory
│   │               │           └── x86_64
│   │               │               ├── linux 1ファイル
│   │               ├── types                35ファイル
│   │               └── util                  6ファイル
│   │                   └── ref               5ファイル
│   │                       └── internal      1ファイル
│   └── test  : テストコード
├── target
```

## Todo
|Todo|ステータス|成果物|
|docの確認|済み|[doc確認.md]|
|標準的なシーケンスの確認|未実施|
|個別クラス確認対しよう

- [ ] 基本アーキテクチャの確認 [4/30]
  - [x] [doc確認](doc%E7%A2%BA%E8%AA%8D.md)[9ファイル] [4/6]
  - [ ] 基本処理の確認 [4/16-4/22]
    - [ ] サンプルのシーケンス作成 [4/16]
    - [ ] 関連クラス確認 [4/22]
  - [ ] 後続作業の予定確認（全256ファイルあるのでサンプリング必要）
  - [ ] その他クラスの確認
    - [ ] ffi 
    - [ ] annotaions
    - [ ] buref
    - [ ] mapper
    - [ ] provider
      - [ ] converts
      - [ ] jffi
        - [ ] platform
            - [ ] x86_65
                - [ ] linux
    - [ ] types
    - [ ] util
        - [ ] ref
            - [ ] internal

    
