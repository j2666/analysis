コンテンツにスキップ
 | 市場サインイン
 
VisualStudioコード>プログラミング言語> Java用デバッガーVisual Studio Codeは初めてですか？ 今すぐ入手してください。
	
Java用デバッガープレビュー
マイクロソフト
|14,046,399インストール
|

(22)
| 無料
VisualStudioCode用の軽量Javaデバッガー
インストールに問題がありますか？
概要バージョン履歴質疑応答評価とレビュー
Java用デバッガー
グリッド GitHubアクション

概要
RedHatによるJavaの言語サポートを拡張するJavaDebugServerに基づく軽量のJavaデバッガー。これにより、ユーザーはVisual Studio Code（VS Code）を使用してJavaコードをデバッグできます。機能のリストは次のとおりです。

起動/添付
ブレークポイント/条件付きブレークポイント/ログポイント
例外
一時停止して続行
ステップイン/アウト/オーバー
変数
コールスタック
スレッド
デバッグコンソール
評価
ホットコード置換
要件
JDK（バージョン1.8.0以降）
VS Code（バージョン1.19.0以降）
Red HatによるJavaの言語サポート（バージョン0.14.0以降）
インストール
VS Codeを開きF1、またはCtrl + Shift + Pを押してコマンドパレットを開き、[ Install Extension ]を選択して、と入力しvscode-java-debugます。

または、VS Code Quick Open（Ctrl + P）を起動し、次のコマンドを貼り付けて、Enterキーを押します。

ext install vscode-java-debug
# 使用する
VSCodeを起動します
Javaプロジェクトを開きます（Maven / Gradle / Eclipse）
Javaファイルを開いて拡張機能をアクティブ化する
デバッグ構成を追加し、launch.jsonを編集します
起動するには：指定しますmainClass
添付するには：指定hostNameしてport
F5を押します
プロジェクトのセットアップで問題が発生した場合は、RedHatによるJavaの言語サポートのドキュメントも確認してください。

# オプション
## Launch
- mainClass（必須）-完全修飾クラス名（[java module name /] com.xyz.MainAppなど）またはプログラムエントリのjavaファイルパス。
- args-プログラムに渡されるコマンドライン引数。"${command:SpecifyProgramArgs}"プログラム引数の入力を求めるために使用します。文字列または文字列の配列を受け入れます。
- sourcePaths-プログラムの追加のソースディレクトリ。デバッガーは、デフォルトでプロジェクト設定からソースコードを検索します。このオプションを使用すると、デバッガーは追加のディレクトリでソースコードを探すことができます。
- modulePaths-JVMを起動するためのモジュールパス。指定しない場合、デバッガーは現在のプロジェクトから自動的に解決します。
- classPaths-JVMを起動するためのクラスパス。指定しない場合、デバッガーは現在のプロジェクトから自動的に解決します。
- encoding-JVMのfile.encoding設定。可能な値はhttps://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.htmlにあります。
- vmArgs--JVMの追加オプションとシステムプロパティ（例：-Xms <size> -Xmx <size> -D <name> = <value>）、文字列または文字列の配列を受け入れます。
- projectName-デバッガーがクラスを検索する優先プロジェクト。異なるプロジェクトでクラス名が重複している可能性があります。この設定は、プログラムの起動時にデバッガーが指定されたメインクラスを探すときにも機能します。ワークスペースに複数のJavaプロジェクトがある場合に必要です。そうでない場合、式の評価と条件付きブレークポイントが機能しない可能性があります。
- cwd-プログラムの作業ディレクトリ。デフォルトは${workspaceFolder}。
- env-プログラムの追加の環境変数。
- envFile-環境変数定義を含むファイルへの絶対パス。
- stopOnEntry-起動後、プログラムを自動的に一時停止します。
- console-プログラムを起動するために指定されたコンソール。java.debug.settings.console指定しない場合は、ユーザー設定 で指定したコンソールを使用してください。
  - internalConsole-VS Codeデバッグコンソール（入力ストリームはサポートされていません）。
  - integratedTerminal-VSCode統合端末。
  - externalTerminal-ユーザー設定で構成できる外部端末。
- shortenCommandLine-プロジェクトに長いクラスパスまたは大きなVM引数がある場合、プログラムを起動するコマンドラインがOSで許可されている最大コマンドライン文字列の制限を超える場合があります。この構成項目は、コマンドラインを短縮するための複数のアプローチを提供します。デフォルトはauto。
  - none-標準のコマンドライン「javaoptionsclassname[args]」を使用してプログラムを起動します。
  - jarmanifest-クラスパスパラメータを一時的なclasspath.jarファイルに生成し、コマンドライン'java -cp classpath.jarclassname[args]'を使用してプログラムを起動します。
  - argfile-一時引数ファイルへのクラスパスパラメータを生成し、コマンドライン'java@argfile[args]'を使用してプログラムを起動します。この値は、Java9以降にのみ適用されます。
  - auto-コマンドラインの長さを自動的に検出し、適切な方法でコマンドラインを短くするかどうかを決定します。
- stepFilters-ステップするときに、指定されたクラスまたはメソッドをスキップします。
    - classNameFilters-[非推奨-置換skipClasses]ステップ時に指定されたクラスをスキップします。クラス名は完全修飾する必要があります。ワイルドカードがサポートされています。
    - skipClasses-ステップするときに指定されたクラスをスキップします。'$JDK'や'$Libraries'などの組み込み変数を使用して、クラスのグループをスキップしたり、java。*、*。Fooなどの特定のクラス名式を追加したりできます。
    - skipSynthetics-ステッピング時に合成メソッドをスキップします。
    - skipStaticInitializers-ステッピング時に静的初期化メソッドをスキップします。
    - skipConstructors-ステップするときにコンストラクターメソッドをスキップします。 
## Attach
- hostName（使用しない限り必須processId）-リモートデバッグ対象のホスト名またはIPアドレス。
- port（使用しない限り必須processId）-リモートデバッグ対象のデバッグポート。
- processId-プロセスピッカーを使用して、アタッチするプロセスを選択するか、プロセスIDを整数として選択します。
  - ${command:PickJavaProcess}-プロセスピッカーを使用して、アタッチするプロセスを選択します。
  - 整数pid-指定されたローカルプロセスに接続します。
- timeout-再接続前のタイムアウト値（ミリ秒単位）（デフォルトは30000ms）。
- sourcePaths-プログラムの追加のソースディレクトリ。デバッガーは、デフォルトでプロジェクト設定からソースコードを検索します。このオプションを使用すると、デバッガーは追加のディレクトリでソースコードを探すことができます。
- projectName-デバッガーがクラスを検索する優先プロジェクト。異なるプロジェクトでクラス名が重複している可能性があります。ワークスペースに複数のJavaプロジェクトがある場合に必要です。そうでない場合、式の評価と条件付きブレークポイントが機能しない可能性があります。
- stepFilters-ステップするときに、指定されたクラスまたはメソッドをスキップします。
  - classNameFilters-[非推奨-置換skipClasses]ステップ時に指定されたクラスをスキップします。クラス名は完全修飾する必要があります。ワイルドカードがサポートされています。
  - skipClasses-ステップするときに指定されたクラスをスキップします。'$JDK'や'$Libraries'などの組み込み変数を使用して、クラスのグループをスキップしたり、java。*、*。Fooなどの特定のクラス名式を追加したりできます。
  - skipSynthetics-ステッピング時に合成メソッドをスキップします。
  - skipStaticInitializers-ステッピング時に静的初期化メソッドをスキップします。
  - skipConstructors-ステップするときにコンストラクターメソッドをスキップします。

## ユーザー設定
- java.debug.logLevel：VSCodeに送信されるデバッガログの最小レベル。デフォルトはwarn。
- java.debug.settings.showHex：「変数」ビューレットに16進形式で数値を表示します。デフォルトはfalse。です。
- java.debug.settings.showStaticVariables：「変数」ビューレットに静的変数を表示します。デフォルトはfalse。です。
- java.debug.settings.showQualifiedNames：「変数」ビューレットに完全修飾クラス名を表示します。デフォルトはfalse。です。
- java.debug.settings.showLogicalStructure：「変数」ビューレットにCollectionクラスとMapクラスの論理構造を表示します。デフォルトはtrue。です。
- java.debug.settings.showToString：「変数」ビューレットの「toString」メソッドをオーバーライドするすべてのクラスの「toString（）」値を表示します。デフォルトはtrue。です。
- java.debug.settings.maxStringLength：「変数」または「デバッグコンソール」ビューレットに表示される文字列の最大長。この長さより長い文字列はトリミングされます。デフォルト0では、トリミングは実行されません。
- java.debug.settings.numericPrecision：「変数」または「デバッグコンソール」ビューレットでフォーマットするときの精度が2倍になります。
- java.debug.settings.hotCodeReplace：デバッグ中に変更されたJavaクラスをリロードします。デフォルトはmanual。です。VSCodeJavajava.autobuild.enabledで無効になっていないことを確認してください。使用法と制限の 詳細については、 wikiページを参照してください。
  - manual -ツールバーをクリックして変更を適用します。
  - auto-コンパイル後に変更を自動的に適用します。
  - never -変更を適用しないでください。
- java.debug.settings.enableRunDebugCodeLens：メインエントリポイントの実行ボタンとデバッグボタンのコードレンズプロバイダーを有効にします。デフォルトはtrueです。
- java.debug.settings.forceBuildBeforeLaunch：Javaプログラムを起動する前に、ワークスペースを強制的にビルドします。デフォルトはtrue。です。
- java.debug.settings.onBuildFailureProceed：ビルドが失敗したときに強制的に続行します。デフォルトはfalseです。
- java.debug.settings.console：Javaプログラムを起動するために指定されたコンソール。デフォルトはintegratedTerminal。特定のデバッグセッション用にコンソールをカスタマイズする場合は、launch.jsonの「console」構成を変更してください。
  - internalConsole-VS Codeデバッグコンソール（入力ストリームはサポートされていません）。
  - integratedTerminal-VSCode統合端末。
  - externalTerminal-ユーザー設定で構成できる外部端末。

- java.debug.settings.exceptionBreakpoint.skipClasses：例外でブレークする場合、指定されたクラスをスキップします。'$JDK'や'$Libraries'などの組み込み変数を使用して、クラスのグループをスキップしたり、java.*などの特定のクラス名式を追加したりできます。
- java.debug.settings.stepping.skipClasses：ステップするときに指定されたクラスをスキップします。'$JDK'や'$Libraries'などの組み込み変数を使用して、クラスのグループをスキップしたり、java。*、*。Fooなどの特定のクラス名式を追加したりできます。
- java.debug.settings.stepping.skipSynthetics：ステッピング時に合成メソッドをスキップします。
- java.debug.settings.stepping.skipStaticInitializers：ステッピング時に静的初期化メソッドをスキップします。
- java.debug.settings.stepping.skipConstructors：ステップするときにコンストラクターメソッドをスキップします。

- java.debug.settings.jdwp.limitOfVariablesPerJdwpRequest：1回のJDWP要求で要求できる変数またはフィールドの最大数。値が高いほど、変数ビューを展開するときにデバッグ対象が要求される頻度は低くなります。また、数値が大きいと、JDWP要求のタイムアウトが発生する可能性があります。デフォルトは100です。
- java.debug.settings.jdwp.requestTimeout：デバッガーがターゲットJVMと通信するときのJDWP要求のタイムアウト（ミリ秒）。デフォルトは3000です。
- java.debug.settings.vmArgs：Javaプログラムを起動するためのデフォルトのVM引数。例えば。'-Xmx1G -ea'を使用して、ヒープサイズを1GBに増やし、アサーションを有効にします。特定のデバッグセッションのVM引数をカスタマイズする場合は、launch.jsonの「vmArgs」構成を変更してください。
- java.silentNotification：通知を使用して進行状況を報告できるかどうかを制御します。trueの場合、代わりにステータスバーを使用して進行状況を報告します。デフォルトはfalse。

上級者向けのヒント：ドキュメントConfiguration.mdには、これらのデバッグ構成の使用方法を示す多くのサンプルが用意されています。確認することをお勧めします。

トラブルシューティング
一般的なエラーについては、トラブルシューティングガイドを参照してください。エンコーディングの問題については、エンコーディングの問題に関するトラブルシューティングガイドを
参照してください。

貢献
問題を修正してコードベースに直接貢献することに興味がある場合は、ドキュメント「貢献する方法」を参照してください。

フィードバックと質問
問題の完全なリストは、IssueTrackerにあります。バグや機能の提案を送信し、コミュニティ主導で参加することができますグリッド

ライセンス
この拡張機能はMITライセンスの下でライセンスされています。

データ/テレメトリ
VS Codeは、使用状況データを収集してMicrosoftに送信し、製品とサービスの改善に役立てます。詳細については、プライバシーに関する声明をお読みください。使用状況データをMicrosoftに送信したくない場合は、telemetry.telemetryLevel設定をに設定できます"off"。詳しくはFAQをご覧ください。
