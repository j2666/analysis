

# docs
├── [ 512]  docs
│   ├── [8.8K]  ComparisonToSimilarProjects.md
│   ├── [3.1K]  FAQ.md
│   ├── [5.4K]  GettingStarted.md
│   ├── [1.6K]  MemoryManagement.md
│   ├── [1.6K]  ProjectsUsingJNR.md
│   ├── [ 546]  README.md
│   ├── [3.5K]  SqueezingPerformance.md
│   ├── [7.4K]  TypeMappings.md
│   └── [8.1K]  WhyUseJNR.md
# libtest
├── [ 512]  libtest
│   ├── [ 839]  Benchmark.c
│   ├── [1.3K]  BufferTest.c
│   ├── [3.1K]  ClosureTest.c
│   ├── [3.2K]  GNUmakefile
│   ├── [1.6K]  GlobalVariable.c
│   ├── [ 868]  LastErrorTest.c
│   ├── [1.9K]  NumberTest.c
│   ├── [1.6K]  PointerNumericTest.c
│   ├── [2.0K]  PointerTest.c
│   ├── [1.5K]  ReferenceTest.c
│   ├── [1.0K]  StringTest.c
│   ├── [ 13K]  StructTest.c
│   ├── [ 512]  struct
│   │   ├── [1.9K]  NestedStruct.c
│   │   ├── [1.8K]  NestedUnion.c
│   │   ├── [1.0K]  NumericStruct.c
│   │   ├── [1.0K]  NumericUnion.c
│   │   └── [1.3K]  StructTypes.h
│   └── [ 690]  types.h

# ソース配下
##  jnr/ffi
│   │               ├── [6.6K]  Address.java
│   │               ├── [1.0K]  CallingConvention.java
│   │               ├── [1.4K]  LastError.java
│   │               ├── [6.1K]  Library.java
│   │               ├── [ 21K]  LibraryLoader.java
│   │               ├── [2.7K]  LibraryOption.java
│   │               ├── [7.7K]  Memory.java
│   │               ├── [5.7K]  NativeLong.java
│   │               ├── [2.8K]  NativeType.java
│   │               ├── [5.0K]  ObjectReferenceManager.java
│   │               ├── [ 32K]  Platform.java
│   │               ├── [ 32K]  Pointer.java
│   │               ├── [6.9K]  Runtime.java
│   │               ├── [ 83K]  Struct.java
│   │               ├── [ 59K]  StructLayout.java
│   │               ├── [1.7K]  Type.java
│   │               ├── [1.2K]  TypeAlias.java
│   │               ├── [1.1K]  Union.java
│   │               ├── [1.5K]  Variable.java
### jnr.ffi.annotations
│   │               ├── [ 512]  annotations
│   │               │   ├── [1.8K]  Clear.java
│   │               │   ├── [1.1K]  Delegate.java
│   │               │   ├── [1.5K]  Direct.java
│   │               │   ├── [1.1K]  Encoding.java
│   │               │   ├── [1.8K]  IgnoreError.java
│   │               │   ├── [1.8K]  In.java
│   │               │   ├── [1.2K]  LongLong.java
│   │               │   ├── [ 974]  Meta.java
│   │               │   ├── [1.1K]  NulTerminate.java
│   │               │   ├── [2.0K]  Out.java
│   │               │   ├── [1.5K]  Pinned.java
│   │               │   ├── [1.1K]  SaveError.java
│   │               │   ├── [ 968]  StdCall.java
│   │               │   ├── [1.1K]  Synchronized.java
│   │               │   ├── [1.2K]  Transient.java
│   │               │   ├── [1.1K]  TypeDefinition.java
│   │               │   └── [ 789]  Variadic.java

#### jnr.ffi.byref
│   │               ├── [ 512]  byref
│   │               │   ├── [1.9K]  AbstractNumberReference.java
│   │               │   ├── [1.3K]  AbstractReference.java
│   │               │   ├── [3.0K]  AddressByReference.java
│   │               │   ├── [2.3K]  ByReference.java
│   │               │   ├── [2.9K]  ByteByReference.java
│   │               │   ├── [2.4K]  DoubleByReference.java
│   │               │   ├── [2.3K]  FloatByReference.java
│   │               │   ├── [2.9K]  IntByReference.java
│   │               │   ├── [3.1K]  LongLongByReference.java
│   │               │   ├── [3.2K]  NativeLongByReference.java
│   │               │   ├── [4.6K]  NumberByReference.java
│   │               │   ├── [2.3K]  PointerByReference.java
│   │               │   └── [3.0K]  ShortByReference.java
### jnr.ffi.mapper
│   │               ├── [ 512]  mapper
│   │               │   ├── [ 792]  AbstractDataConverter.java
│   │               │   ├── [1.0K]  AbstractFromNativeType.java
│   │               │   ├── [1.0K]  AbstractSignatureTypeMapper.java
│   │               │   ├── [1.0K]  AbstractToNativeType.java
│   │               │   ├── [4.6K]  CachingTypeMapper.java
│   │               │   ├── [1.6K]  CompositeFunctionMapper.java
│   │               │   ├── [2.1K]  CompositeTypeMapper.java
│   │               │   ├── [ 812]  DataConverter.java
│   │               │   ├── [3.1K]  DefaultSignatureType.java
│   │               │   ├── [1.8K]  DefaultTypeMapper.java
│   │               │   ├── [1.2K]  FromNativeContext.java
│   │               │   ├── [1.5K]  FromNativeConverter.java
│   │               │   ├── [1.1K]  FromNativeType.java
│   │               │   ├── [1.4K]  FromNativeTypes.java
│   │               │   ├── [2.4K]  FunctionMapper.java
│   │               │   ├── [3.4K]  MethodParameterContext.java
│   │               │   ├── [1.5K]  MethodResultContext.java
│   │               │   ├── [ 956]  SignatureType.java
│   │               │   ├── [ 925]  SignatureTypeMapper.java
│   │               │   ├── [1.4K]  SignatureTypeMapperAdapter.java
│   │               │   ├── [1.2K]  SimpleFunctionMapper.java
│   │               │   ├── [1.7K]  SimpleTypeMapper.java
│   │               │   ├── [1.2K]  ToNativeContext.java
│   │               │   ├── [1.7K]  ToNativeConverter.java
│   │               │   ├── [1.1K]  ToNativeType.java
│   │               │   ├── [1.4K]  ToNativeTypes.java
│   │               │   ├── [2.0K]  TypeMapper.java
│   │               │   └── [ 710]  Util.java
### jnr.ffi.provider
│   │               ├── [ 512]  provider
│   │               │   ├── [ 16K]  AbstractArrayMemoryIO.java
│   │               │   ├── [6.4K]  AbstractBufferMemoryIO.java
│   │               │   ├── [6.5K]  AbstractMemoryIO.java
│   │               │   ├── [3.2K]  AbstractRuntime.java
│   │               │   ├── [1.2K]  BadType.java
│   │               │   ├── [9.0K]  BoundedMemoryIO.java
│   │               │   ├── [ 934]  ClosureManager.java
│   │               │   ├── [2.8K]  DefaultObjectReferenceManager.java
│   │               │   ├── [ 811]  DelegatingMemoryIO.java
│   │               │   ├── [2.6K]  FFIProvider.java
│   │               │   ├── [1.7K]  FromNativeType.java
│   │               │   ├── [1.2K]  IdentityFunctionMapper.java
│   │               │   ├── [4.6K]  InAccessibleMemoryIO.java
│   │               │   ├── [1.3K]  IntPointer.java
│   │               │   ├── [5.5K]  InterfaceScanner.java
│   │               │   ├── [1.8K]  InvalidProvider.java
│   │               │   ├── [2.5K]  InvalidRuntime.java
│   │               │   ├── [1.6K]  InvocationSession.java
│   │               │   ├── [ 834]  Invoker.java
│   │               │   ├── [ 842]  LoadedLibrary.java
│   │               │   ├── [1.4K]  MemoryManager.java
│   │               │   ├── [2.4K]  NativeFunction.java
│   │               │   ├── [2.3K]  NativeInvocationHandler.java
│   │               │   ├── [ 947]  NativeVariable.java
│   │               │   ├── [1.1K]  NullMemoryIO.java
│   │               │   ├── [1.7K]  NullTypeMapper.java
│   │               │   ├── [3.6K]  ParameterFlags.java
│   │               │   ├── [1.2K]  ParameterType.java
│   │               │   ├── [1.2K]  ResultType.java
│   │               │   ├── [5.9K]  ShareMemoryIO.java
│   │               │   ├── [2.0K]  SigType.java
│   │               │   ├── [1.7K]  ToNativeType.java
#### jnr.ffi.provider.converters
│   │               │   ├── [ 512]  converters
│   │               │   │   ├── [3.0K]  BoxedBooleanArrayParameterConverter.java
│   │               │   │   ├── [2.9K]  BoxedByteArrayParameterConverter.java
│   │               │   │   ├── [3.0K]  BoxedDoubleArrayParameterConverter.java
│   │               │   │   ├── [2.9K]  BoxedFloatArrayParameterConverter.java
│   │               │   │   ├── [3.0K]  BoxedIntegerArrayParameterConverter.java
│   │               │   │   ├── [2.9K]  BoxedLong32ArrayParameterConverter.java
│   │               │   │   ├── [3.0K]  BoxedLong64ArrayParameterConverter.java
│   │               │   │   ├── [2.9K]  BoxedShortArrayParameterConverter.java
│   │               │   │   ├── [2.9K]  ByReferenceParameterConverter.java
│   │               │   │   ├── [4.8K]  CharSequenceArrayParameterConverter.java
│   │               │   │   ├── [5.1K]  CharSequenceParameterConverter.java
│   │               │   │   ├── [1.7K]  EnumConverter.java
│   │               │   │   ├── [3.4K]  EnumSetConverter.java
│   │               │   │   ├── [2.9K]  Long32ArrayParameterConverter.java
│   │               │   │   ├── [3.0K]  NativeLong32ArrayParameterConverter.java
│   │               │   │   ├── [3.1K]  NativeLong64ArrayParameterConverter.java
│   │               │   │   ├── [1.8K]  NativeLongConverter.java
│   │               │   │   ├── [3.2K]  Pointer32ArrayParameterConverter.java
│   │               │   │   ├── [3.3K]  Pointer64ArrayParameterConverter.java
│   │               │   │   ├── [3.1K]  StringBufferParameterConverter.java
│   │               │   │   ├── [4.3K]  StringBuilderParameterConverter.java
│   │               │   │   ├── [4.1K]  StringResultConverter.java
│   │               │   │   ├── [6.2K]  StringUtil.java
│   │               │   │   ├── [4.4K]  StructArrayParameterConverter.java
│   │               │   │   ├── [2.4K]  StructByReferenceFromNativeConverter.java
│   │               │   │   └── [1.6K]  StructByReferenceToNativeConverter.java
#### jnr.ffi.provider.jffic
│   │               │   └── [ 512]  jffi
│   │               │       ├── [ 12K]  ARM_64StubCompiler.java
│   │               │       ├── [6.5K]  AbstractA64StubCompiler.java
│   │               │       ├── [1.4K]  AbstractAsmLibraryInterface.java
│   │               │       ├── [ 14K]  AbstractFastNumericMethodGenerator.java
│   │               │       ├── [6.5K]  AbstractX86StubCompiler.java
│   │               │       ├── [2.1K]  AllocatedDirectMemoryIO.java
│   │               │       ├── [5.1K]  AnnotationTypeMapper.java
│   │               │       ├── [1.5K]  ArrayMemoryIO.java
│   │               │       ├── [9.2K]  AsmBuilder.java
│   │               │       ├── [1.5K]  AsmClassLoader.java
│   │               │       ├── [ 16K]  AsmLibraryLoader.java
│   │               │       ├── [8.4K]  AsmRuntime.java
│   │               │       ├── [7.3K]  AsmStructByReferenceFromNativeConverter.java
│   │               │       ├── [ 26K]  AsmUtil.java
│   │               │       ├── [7.2K]  BaseMethodGenerator.java
│   │               │       ├── [ 10K]  BufferMethodGenerator.java
│   │               │       ├── [6.3K]  BufferParameterStrategy.java
│   │               │       ├── [1.7K]  ByteBufferMemoryIO.java
│   │               │       ├── [9.3K]  ClosureFromNativeConverter.java
│   │               │       ├── [2.7K]  ClosureTypeMapper.java
│   │               │       ├── [3.9K]  ClosureUtil.java
│   │               │       ├── [7.7K]  CodegenUtils.java
│   │               │       ├── [5.8K]  ConverterMetaData.java
│   │               │       ├── [ 43K]  DefaultInvokerFactory.java
│   │               │       ├── [7.8K]  DirectMemoryIO.java
│   │               │       ├── [5.4K]  FastIntMethodGenerator.java
│   │               │       ├── [5.1K]  FastLongMethodGenerator.java
│   │               │       ├── [6.5K]  FastNumericMethodGenerator.java
│   │               │       ├── [2.1K]  HeapBufferParameterStrategy.java
│   │               │       ├── [9.7K]  InvokerTypeMapper.java
│   │               │       ├── [ 12K]  InvokerUtil.java
│   │               │       ├── [1.0K]  JNIInvokeInterface.java
│   │               │       ├── [ 13K]  JNINativeInterface.java
│   │               │       ├── [2.8K]  LibraryLoader.java
│   │               │       ├── [ 891]  LocalVariable.java
│   │               │       ├── [1.4K]  LocalVariableAllocator.java
│   │               │       ├── [1.3K]  MemoryUtil.java
│   │               │       ├── [1.2K]  MethodGenerator.java
│   │               │       ├── [8.3K]  NativeClosureFactory.java
│   │               │       ├── [4.9K]  NativeClosureManager.java
│   │               │       ├── [1.2K]  NativeClosurePointer.java
│   │               │       ├── [ 13K]  NativeClosureProxy.java
│   │               │       ├── [1.2K]  NativeFinalizer.java
│   │               │       ├── [1.4K]  NativeFunctionMapperContext.java
│   │               │       ├── [9.1K]  NativeLibrary.java
│   │               │       ├── [1.8K]  NativeLibraryLoader.java
│   │               │       ├── [2.7K]  NativeMemoryManager.java
│   │               │       ├── [8.8K]  NativeRuntime.java
│   │               │       ├── [1021]  NoTrace.java
│   │               │       ├── [ 995]  NoX86.java
│   │               │       ├── [1.3K]  NotImplMethodGenerator.java
│   │               │       ├── [1.4K]  NullObjectParameterStrategy.java
│   │               │       ├── [8.9K]  NumberUtil.java
│   │               │       ├── [1.3K]  ParameterStrategy.java
│   │               │       ├── [1.7K]  PointerParameterStrategy.java
│   │               │       ├── [3.0K]  PrimitiveArrayParameterStrategy.java
│   │               │       ├── [1.2K]  Provider.java
│   │               │       ├── [7.6K]  ReflectionLibraryLoader.java
│   │               │       ├── [ 11K]  ReflectionVariableAccessorGenerator.java
│   │               │       ├── [1.4K]  SimpleNativeContext.java
│   │               │       ├── [ 27K]  SkinnyMethodAdapter.java
│   │               │       ├── [2.8K]  StructByReferenceResultConverterFactory.java
│   │               │       ├── [4.8K]  StubCompiler.java
│   │               │       ├── [ 857]  SymbolNotFoundError.java
│   │               │       ├── [5.4K]  ToNativeOp.java
│   │               │       ├── [5.3K]  TransientNativeMemory.java
│   │               │       ├── [5.3K]  Types.java
│   │               │       ├── [1.0K]  Util.java
│   │               │       ├── [ 10K]  VariableAccessorGenerator.java
│   │               │       ├── [4.5K]  X86Disassembler.java
│   │               │       ├── [ 15K]  X86MethodGenerator.java
│   │               │       ├── [ 11K]  X86_32StubCompiler.java
│   │               │       ├── [ 12K]  X86_64StubCompiler.java
##### jnr.ffi.provider.jffic.platform
│   │               │       └── [ 512]  platform
│   │               │           └── [ 512]  x86_64
│   │               │               ├── [ 512]  linux
│   │               │               │   └── [3.2K]  TypeAliases.java
### jnr.ffi.types
│   │               ├── [ 512]  types
│   │               │   ├── [1.1K]  blkcnt_t.java
│   │               │   ├── [1.1K]  blksize_t.java
│   │               │   ├── [1.1K]  caddr_t.java
│   │               │   ├── [1.1K]  clock_t.java
│   │               │   ├── [1.1K]  dev_t.java
│   │               │   ├── [1.1K]  fsblkcnt_t.java
│   │               │   ├── [1.1K]  fsfilcnt_t.java
│   │               │   ├── [1.1K]  gid_t.java
│   │               │   ├── [1.1K]  id_t.java
│   │               │   ├── [1.1K]  in_addr_t.java
│   │               │   ├── [1.1K]  in_port_t.java
│   │               │   ├── [1.1K]  ino64_t.java
│   │               │   ├── [1.1K]  ino_t.java
│   │               │   ├── [1.1K]  int16_t.java
│   │               │   ├── [1.1K]  int32_t.java
│   │               │   ├── [1.1K]  int64_t.java
│   │               │   ├── [1.1K]  int8_t.java
│   │               │   ├── [1.1K]  intptr_t.java
│   │               │   ├── [1.1K]  key_t.java
│   │               │   ├── [1.1K]  mode_t.java
│   │               │   ├── [1.1K]  nlink_t.java
│   │               │   ├── [1.1K]  off_t.java
│   │               │   ├── [1.1K]  pid_t.java
│   │               │   ├── [1.1K]  rlim_t.java
│   │               │   ├── [1.1K]  sa_family_t.java
│   │               │   ├── [1.1K]  size_t.java
│   │               │   ├── [1.1K]  socklen_t.java
│   │               │   ├── [1.1K]  ssize_t.java
│   │               │   ├── [1.1K]  swblk_t.java
│   │               │   ├── [1.1K]  time_t.java
│   │               │   ├── [1.1K]  u_int16_t.java
│   │               │   ├── [1.1K]  u_int32_t.java
│   │               │   ├── [1.1K]  u_int64_t.java
│   │               │   ├── [1.1K]  u_int8_t.java
│   │               │   ├── [1.1K]  uid_t.java
│   │               │   └── [1.1K]  uintptr_t.java
### jnr.ffi.util
│   │               └── [ 512]  util
│   │                   ├── [1.3K]  AnnotationNameComparator.java
│   │                   ├── [8.9K]  AnnotationProperty.java
│   │                   ├── [9.0K]  AnnotationProxy.java
│   │                   ├── [3.1K]  Annotations.java
│   │                   ├── [6.3K]  BufferUtil.java
│   │                   ├── [4.6K]  EnumMapper.java
│   │                   └── [ 512]  ref
│   │                       ├── [1.5K]  FinalizablePhantomReference.java
│   │                       ├── [1.2K]  FinalizableReference.java
│   │                       ├── [ 11K]  FinalizableReferenceQueue.java
│   │                       ├── [1.4K]  FinalizableSoftReference.java
│   │                       ├── [1.4K]  FinalizableWeakReference.java
│   │                       └── [ 512]  internal
│   │                           └── [9.8K]  Finalizer.java
## テストコード
│   └── [ 512]  test

# その他
├── [ 512]  target
├── [7.1K]  mvnw
├── [ 10K]  pom.xml

