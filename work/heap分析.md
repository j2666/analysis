# ヒープの分析

|見るもの|取得方法|用途|LiveOjbect以外の出力|FullGC実施|
|:--|:--|:--|:--:|:--:|
|ヒープヒストグラム|jcmd プロセスID GC.class_histogram |ヒープ逼迫原因の一時切り分け*1|含まない|×|
|ヒープヒストグラム|jmap -histo プロセスID |同上|含む|×|
|ヒープヒストグラム|jmap -histo:histo プロセスID |同上|含まない（GC後）|〇|
|ヒープダンプ|jcmd プロセスID GC.heap_dump 出力先パス|ヒープヒストグラムでは不明な場合など、詳細分析をする|含まない|実施しない|
|ヒープダンプ|jmap -dump:live, file=出力先パス プロセスID|同上|含まない|〇|
|ヒープダンプ|jmap -dump, file=出力先パス プロセスID|同上|含む|実施しない|
|ヒープダンプ|+HeapDumpOnOutOfMemoryErrorで実行*3|同上|含む|実施しない|

*1 特定のオブジェクトが逼迫しているかどうかを見る。
*2 ヒープダンプを見るツールは以下がある。

 - jhat
 - jvisualvm
 - mat(EclipseLinkMeoryAnalyzerTool)

*3 他のオプション
-XX:HeapDumpPath=./java_pid%p.hprof
-XX:HeapDumpAfterFullGC
-XX:HeapDumpBeforeFullGC
https://docs.oracle.com/javase/jp/11/tools/java.html#GUID-3B1CE181-CD30-4178-9602-230B800D4FAE


## ヒープヒストグラム
jcmd プロセスID GC.class_histogram 

### 用途（Javaパフォーマンス）より
1. アプリケーションで利用されているオブジェクトの数を確認する
   - 有用なケース
     - 特定の型のオブジェクトがメモリ使用量を押し上げる場合に有用
   - 以下はよく見られるらしい
     - Klass(クラスのメタデータ）
     - 頻繁に生成されるもの
       - charの配列  [C)
       - Stringオブジェクト
     - クラスローダーが読み込んだデータを保持するもの
       - バイト列([B)  
       - オブジェクトの配列( [Ljava.lang.Object;)

## ヒープダンプ

***用語説明**
- 保持メモリ量(reationed memory)
  - GCされたときに開放されるメモリ使用量
    - 浅い(shallow)オブジェクトサイズ : 自身のサイズ
    - 深い(deep)オブジェクトサイズ : 参照先も含むサイズ
- ドミネーター(dominator)
  - ヒープの中で保持メモリの多いオブジェクト

matでは以下ができる。
- dominatotr_tree
   - 単独で保持メモリの多いものを上位から表示する
- histogram
  - 大量にあることで逼迫させているオブジェクトがないかを見る
  ※同じ型のものは合算した形で表示される



参考サイト

https://www.greptips.com/posts/1316/

リファレンス
https://docs.oracle.com/javase/jp/11/tools/jcmd.html#GUID-59153599-875E-447D-8D98-0078A5778F05

- MATドキュメント https://eclipsesource.com/blogs/2013/01/21/10-tips-for-using-the-eclipse-memory-analyzer/
  - group by classloader or packageで、
  -  This will allow you to focus on your Objects.


# 検証
## Javaのヒープがどれだけ使われているかを調査する。
- ヒープダンプを取得して、ヒストグラムで分析する。
- 確認ポイント
  - IFを増やすとどうなるか。
  - 関数を増やすとどうなるか。

|No|IF数|関数の数/IF|shallow heap(バイト) |レポート|
|:--|--:|--:|--:|:--|
|1|00|00| 536,576|if00func00.txt|
|2|01|01|1,375,808|if01func01.txt|
|3|01|10|1,375,808|if01func10.txt|
|4|02|01|1,378,656|if02func01.txt|
|5|02|10|1,382,392|if02func10.txt|

- 結果
  - 各種必要なライブラリでヒープを800KB程度度使う
  - IFの数を増やすと24バイト対象クラスの定義分だけ増える



## 分析

No1
|Package / Class                       |   Objects | Shallow Heap | Retained Heap
|--|--|--|--|
|byte[]             |     3,079 |      153,728 |    >= 153,728
|char[]             |        10 |       33,688 |     >= 33,688
|com                |         0 |            0 |              
|int[]              |        99 |        3,160 |      >= 3,160
|java               |     9,470 |      331,528 |              
|- io              |        65 |        2,248 |              
|- lang            |     5,072 |      157,448 |              
|- net             |       136 |        9,536 |              
|- nio             |         8 |          192 |              
|- security        |        34 |        1,016 |              
|- util            |     4,155 |      161,088 |              
|jdk                |       335 |        9,928 |              
|'- internal        |       335 |        9,928 |              
|long[]             |         4 |        1,088 |      >= 1,088
|short[]            |         4 |          320 |        >= 320
|sun                |        89 |        3,136 |              
|Total: 9 entries   |    13,090 |      536,576 |              
--------------------------------------------------------------

No2
|Package / Class                       |   Objects | Shallow Heap | Retained Heap
|--|--|--|--|
|boolean[]                             |         4 |          832 |        >= 832
|byte[]                                |     8,630 |      468,328 |    >= 468,328
|char[]                                |        15 |       33,896 |     >= 33,896
|com                                   |       113 |        3,328 |              
|- example                            |         1 |           24 |              
|  - If01Func01                      |         0 |            0 |         >= 32
|  - If01Func01$HeapTestLib          |         0 |            0 |         >= 24
|  - If01Func01$HeapTestLib$jnr$ffi$0|         1 |           24 |      >= 1,104
|- kenai                              |       112 |        3,304 |              
|  '- jffi                            |       112 |        3,304 |              
|double[]                              |         4 |          328 |        >= 328
|float[]                               |         2 |           80 |         >= 80
|int[]                                 |       372 |       31,896 |     >= 31,896
|int[][]                               |         1 |           56 |        >= 616
|java                                  |    22,205 |      740,928 |              
|- io                                 |       117 |        4,400 |              
|- lang                               |    14,101 |      415,416 |              
|- math                               |         0 |            0 |              
|- net                                |       157 |       10,552 |              
|- nio                                |        57 |        6,296 |              
|- security                           |       293 |        8,952 |              
|- text                               |         0 |            0 |              
|- util                               |     7,480 |      295,312 |              
|jdk                                   |       490 |       15,144 |              
|jnr                                   |     1,754 |       69,504 |              
|- ffi                                |       176 |        5,048 |              
|- x86asm                             |     1,578 |       64,456 |              
|long[]                                |         6 |        1,120 |      >= 1,120
|org                                   |        10 |          344 |              
|'- objectweb                          |        10 |          344 |              
|short[]                               |         5 |          336 |        >= 336
|sun                                   |       243 |        9,688 |              
|Total: 15 entries                     |    33,854 |    1,375,808 |              
---------------------------------------------------------------------------------

No3    
|Package / Class                       |   Objects | Shallow Heap | Retained Heap
|--|--|--|--|    
|boolean[]                             |         4 |          832 |        >= 832
|byte[]                                |     8,630 |      468,328 |    >= 468,328
|char[]                                |        15 |       33,896 |     >= 33,896
|com                                   |       113 |        3,328 |              
|- example                            |         1 |           24 |              
|  |- If01Func10                      |         0 |            0 |         >= 32
|  |- If01Func10$HeapTestLib          |         0 |            0 |         >= 24
|  |- If01Func10$HeapTestLib$jnr$ffi$0|         1 |           24 |      >= 1,104
|  '- Total: 3 entries                |           |              |              
|- kenai                              |       112 |        3,304 |              
|  '- jffi                            |       112 |        3,304 |              
|double[]                              |         4 |          328 |        >= 328
|float[]                               |         2 |           80 |         >= 80
|int[]                                 |       372 |       31,896 |     >= 31,896
|int[][]                               |         1 |           56 |        >= 616
|java                                  |    22,205 |      740,928 |              
|jdk                                   |       490 |       15,144 |              
|jnr                                   |     1,754 |       69,504 |              
|- ffi                                |       176 |        5,048 |              
|- x86asm                             |     1,578 |       64,456 |              
|long[]                                |         6 |        1,120 |      >= 1,120
|org                                   |        10 |          344 |              
|'- objectweb                          |        10 |          344 |              
|short[]                               |         5 |          336 |        >= 336
|sun                                   |       243 |        9,688 |              
|Total: 15 entries                     |    33,854 |    1,375,808 |              
---------------------------------------------------------------------------------



No4
Package / Class                          |   Objects | Shallow Heap | Retained Heap|
|--|--|--|--|--|
|boolean[]                                |         4 |          832 |        >= 832|
|byte[]                                   |     8,636 |      468,632 |    >= 468,632
|char[]                                   |        15 |       33,896 |     >= 33,896
|com                                      |       117 |        3,424 |              
|- example                               |         2 |           48 |              
|  |- If02Func01                         |         0 |            0 |         >= 32
|  |- If02Func01$HeapTestLib             |         0 |            0 |         >= 24
|  |- If02Func01$HeapTestLib$jnr$ffi$0   |         1 |           24 |      >= 1,104
|  |- If02Func01$HeapTestLib2            |         0 |            0 |         >= 24
|  |- If02Func01$HeapTestLib2$jnr$ffi$1  |         1 |           24 |      >= 1,104
|  '- Total: 5 entries                   |           |              |              
|- kenai                                 |       115 |        3,376 |              
|  '- jffi                               |       115 |        3,376 |              
|     |- CallContext                     |         1 |           64 |        >= 168
|     |- CallContextCache                |         1 |           24 |        >= 336
|     |- CallContextCache$CallContextRef |         1 |           40 |         >= 40
|     |- CallContextCache$Signature      |         1 |           32 |         >= 80
|     |- CallContextCache$SingletonHolder|         0 |            0 |        >= 344
|     |- CallingConvention               |         2 |           48 |        >= 112
|     |- CallingConvention[]             |         1 |           24 |         >= 24
|     |- Foreign                         |         1 |           16 |        >= 432
|     |- Foreign$InstanceHolder          |         0 |            0 |         >= 48
|     |- Foreign$InValidInstanceHolder   |         0 |            0 |         >= 24
|     |- Foreign$ValidInstanceHolder     |         1 |           16 |         >= 16
|     |- Function                        |         0 |            0 |              
|     |- HeapInvocationBuffer            |         0 |            0 |         >= 24
|     |- HeapObjectParameterInvoker      |         0 |            0 |         >= 24
|     |- Init                            |         0 |            0 |        >= 240
|     |- Internals                       |         0 |            0 |              
|     |- InvocationBuffer                |         0 |            0 |         >= 24
|     |- Invoker                         |         0 |            0 |         >= 40
|     |- Invoker$LP64                    |         1 |           24 |         >= 32
|     |- Invoker$SingletonHolder         |         0 |            0 |          >= 8
|     |- Library                         |         2 |           64 |        >= 448
|     |- MemoryIO                        |         0 |            0 |        >= 424
|     |- MemoryIO$CheckedNativeImpl      |         0 |            0 |         >= 24
|     |- MemoryIO$NativeImpl             |         0 |            0 |         >= 24
|     |- MemoryIO$NativeImpl32           |         0 |            0 |         >= 24
|     '- Total: 25 of 64 entries; 39 more|           |              |              
|double[]                                 |         4 |          328 |        >= 328
|float[]                                  |         2 |           80 |         >= 80
|int[]                                    |       373 |       31,912 |     >= 31,912
|int[][]                                  |         1 |           56 |        >= 616
|java                                     |    22,267 |      743,128 |              
|- io                                    |       117 |        4,400 |              
|- lang                                  |    14,125 |      416,224 |              
|- math                                  |         0 |            0 |              
|- net                                   |       157 |       10,552 |              
|- nio                                   |        57 |        6,296 |              
|- security                              |       297 |        9,064 |              
|- text                                  |         0 |            0 |              
|- util                                  |     7,514 |      296,592 |              
|jdk                                      |       490 |       15,144 |              
|jnr                                      |     1,761 |       69,736 |              
|long[]                                   |         6 |        1,120 |      >= 1,120
|org                                      |        10 |          344 |              
|short[]                                  |         5 |          336 |        >= 336
|sun                                      |       243 |        9,688 |              
|Total: 15 entries                        |    33,934 |    1,378,656 |              

No5.
Package / Class                        |   Objects | Shallow Heap | Retained Heap
|--|--|--|--|--|
|boolean[]                              |         4 |          832 |        >= 832
|byte[]                                 |     8,678 |      470,240 |    >= 470,240
|char[]                                 |        15 |       33,896 |     >= 33,896
|com                                    |       117 |        3,424 |              
|- example                             |         2 |           48 |              
|  |- If02Func10                       |         0 |            0 |         >= 32
|  |- If02Func10$HeapTestLib           |         0 |            0 |         >= 24
|  |- If02Func10$HeapTestLib$jnr$ffi$0 |         1 |           24 |      >= 1,104
|  |- If02Func10$HeapTestLib2          |         0 |            0 |         >= 24
|  |- If02Func10$HeapTestLib2$jnr$ffi$1|         1 |           24 |      >= 1,104
|  '- Total: 5 entries                 |           |              |              
|- kenai                               |       115 |        3,376 |              
|double[]                               |         4 |          328 |        >= 328
|float[]                                |         2 |           80 |         >= 80
|int[]                                  |       374 |       31,928 |     >= 31,928
|int[][]                                |         1 |           56 |        >= 616
|java                                   |    22,338 |      745,176 |              
|- io                                  |       117 |        4,400 |              
|- lang                                |    14,188 |      417,952 |              
|- math                                |         0 |            0 |              
|- net                                 |       157 |       10,552 |              
|- nio                                 |        57 |        6,296 |              
|- security                            |       301 |        9,176 |              
|- text                                |         0 |            0 |              
|- util                                |     7,518 |      296,800 |              
|jdk                                    |       491 |       15,208 |              
|jnr                                    |     1,761 |       69,736 |              
|long[]                                 |         6 |        1,120 |      >= 1,120
|org                                    |        10 |          344 |              
|short[]                                |         5 |          336 |        >= 336
|sun                                    |       243 |        9,688 |              
|Total: 15 entries                      |    34,049 |    1,382,392 |              










