# Project Panama

参考サイト


jdk19に対応したチュートリアル的なサイト
- https://foojay.io/today/project-panama-for-newbies-part-1/

こちらも参照したが古い情報(JDK17)だった
- https://logico-jp.io/2020/12/06/project-panama-and-jextract/
- https://yukihane.github.io/blog/202110/08/hello-project-panama-on-java17/


- previewってどういう扱いか調べた
  - フィードバックをもらって修正があるので、過去の実績から見ても、2回ぐらいはpreviewはある。
  https://blogs.oracle.com/otnjp/post/the-role-of-previews-in-java-14-java-15-java-16-and-beyond-ja
  - 記載なかったが、JEPについて参考になった。
   https://tech.pjin.jp/blog/2017/11/08/知っておきたい！javaの仕様ってどうやって決まる/

## 歴史
https://foojay.io/today/project-panama-for-newbies-part-1/

- 2022年現在 OpenJDK19から JEP 424としてプレビュー版となっている。※変更の可能性がある。
  - パッケージ名は java.lang.foreign に変わっている
  - プレビュー機能は、過去の傾向から見ても正式なリリースになるのは、2世代後になる。その間APIの変更はあるので、使うとしてもOpenJDK21(LTS)からがよさそう
    - 参考Oraccle Java SEのロードマップによる21
      https://www.oracle.com/jp/java/technologies/java-se-support-roadmap.html
      - GA 2023年9月
      - Premier Support 2028年9月
      - Extended Support 2031年9月

### JEP424について
https://openjdk.org/jeps/424

424は以下を合わせたもの
- 2つのインキュベーションAPIであるForeign-Memory Access API（JEP 370、383 、および393 ）
- Foreign Linker API（JEP 389)


## 使い方

以下が導入されていること
- 前提
  - jextract
  - opnejdk19


### コンパイル、ビルド方法
jdk-19を使い、以下のオプションを付けて実行する。
jextractの環境設定まで終わらせたら、結果的にjdk-19を使える環境になっている。
- コンパイルして実行する場合
  - コンパイル
    - javac --release 19 --enable-preview
  - 実行時
    - java -enable-native-access=ALL-UNNAMED --enable-preview 実行クラス
- ソースを直接実行する場合
  - java -enable-native-access=ALL-UNNAMED --enable-preview --source 19 ソースファイル名
  
※releaseフラグの代わりに、sourceフラグでもOK。
jextractは含まれていないので別途ダウンロードする必要がある

### 環境設定方法

- 前提
  - Ubunt20.04の環境で実行
  - Githubにユーザ登録、sshの鍵登録していること

以下は、参考サイトをもとに[jextractのビルド](https://foojay.io/today/building-project-panamas-jextract-tool-by-yourself/)をしたものをあとで起こしたもの。


```shell-session

# LLVMの13を入れる（aptで入るものはバージョンが古く、ビルドが通らない）
# パスは適宜見直す https://github.com/llvm/llvm-project/releases/tag/llvmorg-13.0.0
$ wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz

# tarで解凍する
$ tar -xvf clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar 

# java17のインストール(なぜか19でビルドしようとするとうまくいかなかったので、17を入れた）
sudo apt install openjdk-17-jdk-headless

# jdk19のダウンロード
# "https://jdk.java.net/panama/"から取得
$ wget <linux用のファイル>
$ tar -xvzf <ダウンロードして来たjdk19のファイル>

## jextractの取得
$ git clone git@github.com:openjdk/jextract.git

## jextractのビルド
# jdk_19_home_dirは、19の解凍先フォルダ(最後には/をつけない)
# libclang_dirは、ダウンロードしてきたllvm13の解凍先(最後には/をつけない)
# 例 
# $ sh ./gradlew -Pjdk19_home=/home/kogakz/jdk-19 -Pllvm_home=/home/kogakz/llvm13 clean verify 

$ cd jextract
$ sh ./gradlew -Pjdk19_home=<jdk19_home_dir> -Pllvm_home=<libclang_dir> clean verify
```

jextractを実行するときの環境変数の設定として、JAVA_HOME,PATHを指定する。

- 注意
  - jextractやopenjdk19を使う時だけ設定すること。
  -   build/jextract/binの下にjdk19のjava,javacなどのコピーがされているので、java,javacのバージョンも変わってしまう(jextract配下のファイル配置参照）
```shell
$ export JAVA_HOME=<parent directory>//jextract/build/jextract
$ export PATH="$JAVA_HOME/bin:$PATH"
```


** jextract配下のファイル配置 **

```shell-session
jextract/build/jextract
├── bin
│   ├── java
│   ├── javac
│   ├── jextract
│   ├── keytool
│   └── serialver
├── conf
│   ├── jextract
│   │   ├── __clang_cuda_builtin_vars.h
# 省略
│   │   └── xtestintrin.h
│   ├── net.properties
│   ├── sdp
│   │   └── sdp.conf.template
│   └── security
│       ├── java.policy
│       ├── java.security
│       └── policy
│           ├── README.txt
#省略
├── include
│   ├── classfile_constants.h
#省略
├── legal
│   ├── java.base
#省略
│   ├── java.compiler
#省略
│   ├── jdk.compiler
#省略
│   └── jdk.zipfs
#省略
├── lib
│   ├── classlist
│   ├── ct.sym
│   ├── jexec
│   ├── jrt-fs.jar
│   ├── jspawnhelper
│   ├── jvm.cfg
│   ├── libclang.so
│   ├── libclang.so.13
│   ├── libclang.so.13.0.0
│   ├── libjava.so
│   ├── libjimage.so
│   ├── libjli.so
│   ├── libjsig.so
│   ├── libnet.so
│   ├── libnio.so
│   ├── libsyslookup.so
│   ├── libverify.so
│   ├── libzip.so
│   ├── modules
│   ├── security
│   │   ├── blocked.certs
│   │   ├── cacerts
│   │   ├── default.policy
│   │   └── public_suffix_list.dat
│   ├── server
│   │   ├── libjsig.so
│   │   └── libjvm.so
│   └── tzdb.dat
└── release
```

### Panamaを使った実行例（Hello World)

**Step1:** ヘッダファイルのパスの調査方法

```shell-session 
$ gcc -H -fsyntax-only heapTestLib.cpp -I ../../include/
. /usr/include/stdio.h
.. /usr/include/x86_64-linux-gnu/bits/libc-header-start.h
:
```
という感じで出るので、1階層目（.が一つのもの）を選ぶ

**Step 2:** jextractを使ってヘッダファイルからJavaのコードを生成する

1. --source ソースを生成する。指定するとclassが出なかった
2. --output 出力先ディレクトリ
3. -t パッケージ名
4. -I ヘッダファイルのパス
5. -l ライブラリ名
**stdioは、下記例のようにライブラリ名を追加しなくても実行できるが、自作ライブラリの場合はこのオプションをつけないと実行時に"UnsatisfiedLinkError"が発生した。**

```text
### 実行時エラーになった時のメッセージ
Exception in thread "main" java.lang.UnsatisfiedLinkError: unresolved symbol: func01
        at com.sample.RuntimeHelper.requireNonNull(RuntimeHelper.java:47)
        at com.sample.loadSampleLib_h.func01$MH(loadSampleLib_h.java:2590)
        at com.sample.loadSampleLib_h.func01(loadSampleLib_h.java:2593)
        at LoadSample.main(LoadSample.java:11)
```


```shell-session
$ jextract --output classes -t org.unix -I /usr/include /usr/include/stdio.h
$ tree classes/
classes/
└── org
    └── unix
        ├── Constants$root.class
        ├── FILE.class
        ├── RuntimeHelper$VarargsInvoker.class
        ├── RuntimeHelper.class
        ├── _G_fpos64_t.class
        ├── _G_fpos_t.class
        ├── _IO_FILE.class
        ├── __FILE.class
        ├── __fpos64_t.class
        ├── __fpos_t.class
        ├── __fsid_t.class
        ├── __mbstate_t$__value.class
        ├── __mbstate_t.class
        ├── constants$0.class
        ├── constants$1.class
        ├── constants$10.class
        ├── constants$11.class
        ├── constants$12.class
        ├── constants$13.class
        ├── constants$14.class
        ├── constants$15.class
        ├── constants$2.class
        ├── constants$3.class
        ├── constants$4.class
        ├── constants$5.class
        ├── constants$6.class
        ├── constants$7.class
        ├── constants$8.class
        ├── constants$9.class
        ├── fpos_t.class
        └── stdio_h.class

2 directories, 31 files
```

ソースファイル出力の実行例

```shell-session
$ jextract --source --output src -t org.unix -I /usr/include /usr/include/stdio.h
$ tree src/
src/
└── org
    └── unix
        ├── Constants$root.java
        ├── FILE.java
        ├── RuntimeHelper.java
        ├── _G_fpos64_t.java
        ├── _G_fpos_t.java
        ├── _IO_FILE.java
        ├── __FILE.java
        ├── __fpos64_t.java
        ├── __fpos_t.java
        ├── __fsid_t.java
        ├── __mbstate_t.java
        ├── constants$0.java
        ├── constants$1.java
        ├── constants$10.java
        ├── constants$11.java
        ├── constants$12.java
        ├── constants$13.java
        ├── constants$14.java
        ├── constants$15.java
        ├── constants$2.java
        ├── constants$3.java
        ├── constants$4.java
        ├── constants$5.java
        ├── constants$6.java
        ├── constants$7.java
        ├── constants$8.java
        ├── constants$9.java
        ├── fpos_t.java
        └── stdio_h.java
```


**Step 3:** 生成した関数を呼び出すクラスの実装例

```java
import java.lang.foreign.MemorySegment; //static不要
import java.lang.foreign.MemorySession; //static不要
import static org.unix.stdio_h.printf;
public class HelloWorld {
    public static void main(String[] args) {
      // メソッド名が間違っていた。
      // newConfined　-> openConfined※
       try (var memorySession = MemorySession.openConfined()) {
           // Javaの文字列をCの文字列として扱うためにMemorySegmetに変換する
           // char*は別にあるが、文字列を渡すときはお作法としてこれを使うらしい
           MemorySegment cString = memorySession.allocateUtf8String("Hello World! Panama style\n");
           printf(cString);
           //flushしないと前後することがある
           //防止するためのコードは以下のようなもの
           fflush(NULL());

           System.out.println("Java System.out\n");
       }
    }
}
```
memorySessionのもとになったResourceScopeのメソッド名が[newConfinedScope](https://github.com/mcimadamore/jdk/blob/10767bc0529682390b5f8a42bc8154892a39b35d/src/jdk.incubator.foreign/share/classes/jdk/incubator/foreign/ResourceScope.java)だったので記事を書いた人の修正漏れだった



**Step 4:** プログラムの実行

|   No | パラメータ                         | 説明                                                                    |
| ---: | :--------------------------------- | :---------------------------------------------------------------------- |
|    1 | --enable-native-access=ALL-UNNAMED | モジュールではない物からネイティブアクセスの呼び出しを有効にしている。※ |
|    2 | --enable-preview                   | プレビュー機能の有効化                                                  |
|    3 | --release 19                        | 指定されたJava SEリリースとソースの互換性を保持                         |
[jdk19へのPull Requestより](https://github.com/openjdk/jdk/pull/7888)
  - foreing functionを有効化するために必要
  - モジュールであれば、モジュール名を指定する。
  - 今回はモジュールとして作っていないので、ALL-UNNAMEDを指定している。
  - より安全にするためにモジュールとして作成してそこだけで許可するといったことを想定している？

```shell-session
# classpathの実際のパスは、jextractで出力先に指定したclassファイルの
# 出力先ディレクトリのルートに変更する
$ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --release 19 HelloWorld.java
Note: HelloWorld.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
Hello World! Panama style
```

**Step 5:** Cの文字列からJavaの文字列への変換例

```java

import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;

public class C2J {
    public static void main(String[] args) {
       try (var memorySession =  MemorySession.openConfined()) {
           // Cの文字列を用意
           MemorySegment cString = memorySession.allocateUtf8String("Panama");
           String string = cString.getUtf8String(0);
           System.out.printf("Hello, %s from a C string. \n", string);
       }
    }
}
```



**Step 6:** Cのプリミティブ型の作成

参考にしたドキュメント（非公式）では、ValueLayout.JAVA_DOUBLEではなく、jextractを使って対象関数と一緒に生成されるC_DBOULEを使用するとあった。ソースを見ると以下のようになっている。
ValueLayoutのサイズはJAVA_DOUBLEから変えているよう見えるが、

```java
package org.unix;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.VarHandle;
import java.nio.ByteOrder;
import java.lang.foreign.*;
import static java.lang.foreign.ValueLayout.*;

public class Constants$root {
    static final  OfBoolean C_BOOL$LAYOUT = JAVA_BOOLEAN;
    static final  OfByte C_CHAR$LAYOUT = JAVA_BYTE;
    static final  OfShort C_SHORT$LAYOUT = JAVA_SHORT.withBitAlignment(16);
    static final  OfInt C_INT$LAYOUT = JAVA_INT.withBitAlignment(32);
    static final  OfLong C_LONG$LAYOUT = JAVA_LONG.withBitAlignment(64);
    //VALUELAYOUTにない。
    //CのLong Long型は64ビット幅で、JAVA_LONGが64ビットかつ
    static final  OfLong C_LONG_LONG$LAYOUT = JAVA_LONG.withBitAlignment(64);  
    static final  OfFloat C_FLOAT$LAYOUT = JAVA_FLOAT.withBitAlignment(32);
    static final  OfDouble C_DOUBLE$LAYOUT = JAVA_DOUBLE.withBitAlignment(64);
    static final  OfAddress C_POINTER$LAYOUT = ADDRESS.withBitAlignment(64);    //VALUELAYOUTだと実行時だが固定
}
```

https://github.com/openjdk/jdk/blob/master/src/java.base/share/classes/java/lang/foreign/ValueLayout.java

```java
    public static final OfAddress ADDRESS = new OfAddress(ByteOrder.nativeOrder()).withBitAlignment(ValueLayout.ADDRESS_SIZE_BITS);

    public static final OfBoolean JAVA_BOOLEAN = new OfBoolean(ByteOrder.nativeOrder()).withBitAlignment(8);

    public static final OfByte JAVA_BYTE = new OfByte(ByteOrder.nativeOrder()).withBitAlignment(8);

    public static final OfChar JAVA_CHAR = new OfChar(ByteOrder.nativeOrder()).withBitAlignment(16);
    public static final OfShort JAVA_SHORT = new OfShort(ByteOrder.nativeOrder()).withBitAlignment(16);

    public static final OfInt JAVA_INT = new OfInt(ByteOrder.nativeOrder()).withBitAlignment(32);
    public static final OfLong JAVA_LONG = new OfLong(ByteOrder.nativeOrder()).withBitAlignment(64);


    public static final OfFloat JAVA_FLOAT = new OfFloat(ByteOrder.nativeOrder()).withBitAlignment(32);
    public static final OfDouble JAVA_DOUBLE = new OfDouble(ByteOrder.nativeOrder()).withBitAlignment(64);
```



1. 変数の生成（メモリ割り当て）
2. 引数の受け渡し

```java
import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static org.unix.stdio_h.printf;
import static org.unix.stdio_h.C_DOUBLE;

public class CNativeType {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

          // MemorySegmentに変数の生成（メモリの割り当て)
          // 第1引数：ネイティブデータのサイズ
          // 第2引数：アサインしたいデータ(Javaのprimitive型を指定する)
          var cDouble = memorySession.allocate(C_DOUBLE, Math.PI);
          var msgStr = memorySession.allocateUtf8String("A slice of %f \n");

          // 引数の受け渡し
          printf(msgStr, cDouble.get(C_DOUBLE, 0));
        }
    }
}
```
実行方法と実行結果は以下の通り

```shell-session
$ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 CNativeType.java

A slice of 3.141593
```

これだと、微妙という記載があったのだが、根拠が不明

```java
import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static org.unix.stdio_h.printf;
import java.lang.foreign.ValueLayout;

public class CJavaType {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            var cDouble = memorySession.allocate(ValueLayout.JAVA_DOUBLE, Math.PI);
            var msgStr = memorySession.allocateUtf8String("A slice of %f \n");
            printf(msgStr, cDouble.get(ValueLayout.JAVA_DOUBLE, 0));
        }
    }
}

```




**Step 7:** Cの配列型の作成

```java
import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static org.unix.stdio_h.printf;
import static org.unix.stdio_h.C_DOUBLE;

public class CArray {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            System.out.println("An array of data");
            MemorySegment cDoubleArray = memorySession.allocateArray(C_DOUBLE, new double[] {
                    1.0, 2.0, 3.0, 4.0,
                    1.0, 1.0, 1.0, 1.0,
                    3.0, 4.0, 5.0, 6.0,
                    5.0, 6.0, 7.0, 8.0
            });
            for (long i = 0; i < (4 * 4); i++) {
                if (i > 0 && i % 4 == 0) {
                    System.out.println();
                }
                // 配置をC_DOUBLEのサイズ分ずらしながら、データを取得
                System.out.printf(" %f ", cDoubleArray.get(C_DOUBLE, i * 8));

                // エラーケース1
                //  間違った書式で実行すると以下のようなエラーがが出る
                System.out.printf(" %d ", cDoubleArray.get(C_DOUBLE, i * 8));
                // Exception in thread "main" java.util.IllegalFormatConversionException: d != java.lang.Double
                //  at java.base/java.util.Formatter$FormatSpecifier.failConversion(Formatter.java:4510)
                //  at java.base/java.util.Formatter$FormatSpecifier.printInteger(Formatter.java:3055)
                //  at java.base/java.util.Formatter$FormatSpecifier.print(Formatter.java:3010)
                //  at java.base/java.util.Formatter.format(Formatter.java:2781)
                //  at java.base/java.io.PrintStream.implFormat(PrintStream.java:1366)
                //  at java.base/java.io.PrintStream.format(PrintStream.java:1345)
                //  at java.base/java.io.PrintStream.printf(PrintStream.java:1244)

                // エラーケース2 
                //  間違った型に代入するようにすると、以下のようなエラーがでる（コンパイル時にも）
                // CArray.java:24: error: incompatible types: possible lossy conversion from double to int
                int l = cDoubleArray.get(C_DOUBLE, i * 8);

            }
        }
    }
}
```

**Step 7:** [ポインタ、構造体の使い方](https://foojay.io/today/project-panama-for-newbies-part-2/)
次回にする


**Step 8:** [jextractが生成するコードの確認](https://foojay.io/today/project-panama-for-newbies-part-3/)
次回にする

**Step 9:** [CALL Back関数の使い方](https://foojay.io/today/project-panama-for-newbies-part-4/)
次回にする


## 自前ライブラリでの実行例

### jextractでのクラス生成

基にするヘッダーファイルの定義

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
    bool func01(char *arg01);
    void func02(int arg01);
    int func03(char arg01[10][10]);
#ifdef __cplusplus
}
```

```shell-session
$ jextract --output classes -t com.sample -I /usr/include /home/kogakz/cpplib/include/loadSampleLib.h  -l /usr/local/lib/libloadSample.so
WARNING: skipping strtold because of unsupported type usage: long double
WARNING: skipping qecvt because of unsupported type usage: long double
WARNING: skipping qfcvt because of unsupported type usage: long double
WARNING: skipping qgcvt because of unsupported type usage: long double
WARNING: skipping qecvt_r because of unsupported type usage: long double
WARNING: skipping qfcvt_r because of unsupported type usage: long double
```

### 以下実装例を試す
1. 正常系
2. 型誤り1
3. 型誤り2
4. 関数名誤り
5. 引数の数誤り


### 正常系

正しい実装
```java
package experiment;

import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static com.sample.loadSampleLib_h.*;

/**
 * 実行方法
 * $cd /home/kogakz/tmp/
 * $
 * $  jextract --output classes -t org.unix -I /usr/include /usr/include/stdio.h
 * $ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 experiment/OKSample.java 
 */

public class LoadSample {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            MemorySegment cString = memorySession.allocateUtf8String("Hello World! Panama style\n");
            System.out.println("func01 exec\n");
            boolean resultValue = false;
            resultValue = func01(cString);

            System.out.println("func02 exec\n");
            func02(1);


        }
    }
}
```

```shell-session
$java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 experiment/OKSample.java 
Note: experiment/OKSample.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
func01 exec

Hello World! Panama style

func02 exec

arg01=1
```

#### 型誤り1

**実装例**
- 引数の型誤り
  - func01->Stringをint
  - intをdluble

```java
package experiment;

import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static com.sample.loadSampleLib_h.*;

//型誤り
public class NGSample1 {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            int i = 1;
            System.out.println("func01 exec\n");

            boolean resultValue = false;
            resultValue = func01(i);

            System.out.println("func02 exec\n");
            double val = 0.0;
            func02(val);

        }
    }
}

```

**実行結果**

コンパイルした場合
```shell-session
 javac  --enable-preview --source 19 experiment/NGSample1.java 
experiment/NGSample1.java:16: error: incompatible types: int cannot be converted to Addressable
            resultValue = func01(i);
                                 ^
experiment/NGSample1.java:20: error: incompatible types: possible lossy conversion from double to int
            func02(val);
                   ^

```

javaコマンドで直接実行した場合

```shell-session
$ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 experiment/NGSample1.java 
experiment/NGSample1.java:16: error: method func01 in class loadSampleLib_h cannot be applied to given types;
            resultValue = func01(i);
                          ^
  required: Addressable
  found:    int
  reason: argument mismatch; int cannot be converted to Addressable
experiment/NGSample1.java:20: error: method func02 in class loadSampleLib_h cannot be applied to given types;
            func02(val);
            ^
  required: int
  found:    double
  reason: argument mismatch; possible lossy conversion from double to int
Note: experiment/NGSample1.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
2 errors
error: compilation failed
```

#### 型誤り2

**実装例**
- 返り値の型
  - boolean → double

```java
package experiment;

import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static com.sample.loadSampleLib_h.*;

//戻り値の型誤り
public class LoadSample {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            MemorySegment cString = memorySession.allocateUtf8String("Hello World! Panama style\n");
            System.out.println("func01 exec\n");
            double  resultValue;
            resultValue = func01(cString);

            System.out.println("func02 exec\n");
            func02(1);

        }
    }
}
```

**実行結果**

コンパイル実行例
```shell-session
$ javac  --enable-preview --source 19 experiment/NGSample2.java 
experiment/NGSample2.java:15: error: incompatible types: boolean cannot be converted to double
            resultValue = func01(cString);
                                ^
Note: experiment/NGSample2.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
1 error

```

javaで直接実行した場合の例
```shell-session
c$ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 experiment/NGSample2.java 
experiment/NGSample2.java:15: error: incompatible types: boolean cannot be converted to double
            resultValue = func01(cString);
                                ^
Note: experiment/NGSample2.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
1 error
error: compilation failed
```


#### 関数名誤り
**実装例**
- 関数名
  - func01 -> func03
  - func02 -> func04
```java
package experiment;

import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static com.sample.loadSampleLib_h.*;

//関数名誤り
public class NGSample3 {

    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            MemorySegment cString = memorySession.allocateUtf8String("Hello World! Panama style\n");
            System.out.println("func01 exec\n");
            boolean resultValue = false;
            resultValue = func03(cString);

            System.out.println("func02 exec\n");
            func04(1);

        }
    }
}
```

**実行結果**

コンパイル結果
```shell-session
$ javac  --enable-preview --source 19 experiment/NGSample3.java 
experiment/NGSample3.java:16: error: incompatible types: int cannot be converted to boolean
            resultValue = func03(cString);
                                ^
experiment/NGSample3.java:19: error: cannot find symbol
            func04(1);
            ^
  symbol:   method func04(int)
  location: class NGSample3
```

javaコマンドで直接実行した場合
```shell-session
$ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 experiment/NGSample3.java 
experiment/NGSample3.java:16: error: incompatible types: int cannot be converted to boolean
            resultValue = func03(cString);
                                ^
experiment/NGSample3.java:19: error: cannot find symbol
            func04(1);
            ^
  symbol:   method func04(int)
  location: class NGSample3
Note: experiment/NGSample3.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
2 errors
error: compilation failed
```


#### 引数の数誤り
**実装例**
```java
package experiment;

import java.lang.foreign.MemorySegment;
import java.lang.foreign.MemorySession;
import static com.sample.loadSampleLib_h.*;

//関数名誤り
public class NGSample4 {
    public static void main(String[] args) {
        try (var memorySession = MemorySession.openConfined()) {

            MemorySegment cString = memorySession.allocateUtf8String("Hello World! Panama style\n");
            System.out.println("func01 exec\n");
            boolean resultValue = false;
            
            int i = 1;
            resultValue = func01(cString,i);

            System.out.println("func02 exec\n");
            func02(1,i);

        }
    }
}
```

**実行結果**

コンパイル結果
```shell-session
$ javac  --enable-preview --source 19 experiment/NGSample4.java 
experiment/NGSample4.java:17: error: method func01 in class loadSampleLib_h cannot be applied to given types;
            resultValue = func01(cString,i);
                          ^
  required: Addressable
  found:    MemorySegment,int
  reason: actual and formal argument lists differ in length
experiment/NGSample4.java:20: error: method func02 in class loadSampleLib_h cannot be applied to given types;
            func02(1,i);
            ^
  required: int
  found:    int,int
  reason: actual and formal argument lists differ in length
Note: experiment/NGSample4.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
2 errors
```


```shell-session
$ java -classpath /home/kogakz/tmp/classes/ --enable-native-access=ALL-UNNAMED --enable-preview --source 19 experiment/NGSample4.java 
experiment/NGSample4.java:17: error: method func01 in class loadSampleLib_h cannot be applied to given types;
            resultValue = func01(cString,i);
                          ^
  required: Addressable
  found:    MemorySegment,int
  reason: actual and formal argument lists differ in length
experiment/NGSample4.java:20: error: method func02 in class loadSampleLib_h cannot be applied to given types;
            func02(1,i);
            ^
  required: int
  found:    int,int
  reason: actual and formal argument lists differ in length
Note: experiment/NGSample4.java uses preview features of Java SE 19.
Note: Recompile with -Xlint:preview for details.
2 errors
error: compilation failed
```

