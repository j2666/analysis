/*
 * Copyright (C) 2012 Wayne Meissner
 *
 * This file is part of the JNR project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jnr.ffi.mapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

import static jnr.ffi.util.Annotations.sortedAnnotationCollection;

/**
*
*/
public final class DefaultSignatureType implements SignatureType {
    // コンストラクタに渡されるdeclaredClass（Methodo.getReturnType)を内部変数で保持
    private final Class declaredClass;
    // コンストラクタに渡されるannotationをソートして設定される
    private final Collection<Annotation> annotations;
    // コンストラクタで渡されるresultContextが設定されている
    private final Type genericType;

    /**
     * 受け取ったパラメータを後で使うために内部変数にセット。annotationsはソートしなおす
     * @param declaredClass Methodo.getReturnType
     * @param annotations
     * @param genericType resultContextが設定されている
     */
    public DefaultSignatureType(Class declaredClass, Collection<Annotation> annotations, Type genericType) {
        this.declaredClass = declaredClass;
        this.annotations = sortedAnnotationCollection(annotations);
        this.genericType = genericType;
    }

    public Class getDeclaredType() {
        return declaredClass;
    }

    public Collection<Annotation> getAnnotations() {
        return annotations;
    }

    public Type getGenericType() {
        return genericType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DefaultSignatureType signature = (DefaultSignatureType) o;

        return declaredClass == signature.declaredClass
                && genericType.equals(signature.genericType)
                && annotations.equals(signature.annotations)
                ;
    }

    @Override
    public int hashCode() {
        int result = declaredClass.hashCode();
        result = 31 * result + annotations.hashCode();
        if (genericType != null) result = 31 * result + genericType.hashCode();
        return result;
    }

    /**
     * DefaultSignatureTypeを生成するだけ
     * @param type
     * @param context
     * @return
     */
    public static DefaultSignatureType create(Class type, FromNativeContext context) {
        Type genericType = !type.isPrimitive() && context instanceof MethodResultContext
                ? ((MethodResultContext) context).getMethod().getGenericReturnType() : type;
        return new DefaultSignatureType(type, context.getAnnotations(), genericType);
    }

    public static DefaultSignatureType create(Class type, ToNativeContext context) {
        Type genericType = type;
        // メソッド呼び出し処理のDefaultSignatureType生成時に呼び出されている場合
        // genericTypeをメソッドの自身のJava型で上書きしているが、
        // 何のためにやっているかはわかってない。
        if (!type.isPrimitive() && context instanceof MethodParameterContext) {
            MethodParameterContext methodParameterContext = (MethodParameterContext) context;
            genericType = methodParameterContext.getMethod().getGenericParameterTypes()[methodParameterContext.getParameterIndex()];
        }

        return new DefaultSignatureType(type, context.getAnnotations(), genericType);
    }
}
