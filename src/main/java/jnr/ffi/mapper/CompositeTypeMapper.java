/*
 * Copyright (C) 2012 Wayne Meissner
 *
 * This file is part of the JNR project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jnr.ffi.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * 複数の関数の引数の対応リストを管理する。受け渡し先で変更されないように読み取り専用のリスト（unmodifiableList)とするためのクラス
 * InvokeUtil.getParameterTypesで使われる場合
 * signatureTypeMappersは以下のようになっている
 *  [0] SingatureTYpeMappaerAdapter
 *  [1] InvokerTypeMapper
 *  [2] AnnotationTypeMapper
 */
public final class CompositeTypeMapper implements SignatureTypeMapper {
    private final Collection<SignatureTypeMapper> signatureTypeMappers;

    public CompositeTypeMapper(SignatureTypeMapper... signatureTypeMappers) {
        this.signatureTypeMappers = Collections.unmodifiableList(Arrays.asList(signatureTypeMappers.clone()));
    }

    /**
     * 引数で渡されたものを読みとり専用として
     * @param signatureTypeMappers
     */
    public CompositeTypeMapper(Collection<SignatureTypeMapper> signatureTypeMappers) {
        this.signatureTypeMappers = Collections.unmodifiableList(new ArrayList<SignatureTypeMapper>(signatureTypeMappers));
    }

    @Override
    public FromNativeType getFromNativeType(SignatureType type, FromNativeContext context) {
        for (SignatureTypeMapper m : signatureTypeMappers) {
            FromNativeType fromNativeType = m.getFromNativeType(type, context);
            if (fromNativeType != null) {
                return fromNativeType;
            }
        }

        return null;
    }

    /**
     * InvokeUtil.getParameterTypesから呼び出される場合
     * クラス生成時に渡された以下のgetToNativeTypeを順番に呼び出し、
     * いずれかで、ToNativeTypeで取得出来たら、即時取得結果を返却する
     *      1. SingatureTypeMappaerAdapter
     *      2. InvokerTypeMapper
     *      3. AnnotationTypeMapper
     *  いずれも取得できない場合はnullを返却する。
     * @param type signatureTypeは引数のクラス、アノテーション、型の情報を保持している
     * @param context MethodParameterContextが渡される
     * @return
     */
    @Override
    public ToNativeType getToNativeType(SignatureType type, ToNativeContext context) {
        //signatureTypeMappersは以下のようになっている
        // [0] SingatureTypeMappaerAdapter
        // [1] InvokerTypeMapper
        // [2] AnnotationTypeMapper
        for (SignatureTypeMapper m : signatureTypeMappers) {
            // 各Mapperから、引数のtypeと、MethodParameterContextを渡して、
            // ToNativeTypeを取得して取得出来たら、それを返す
            ToNativeType toNativeType = m.getToNativeType(type, context);
            if (toNativeType != null) {
                return toNativeType;
            }
        }

        return null;
    }
}
