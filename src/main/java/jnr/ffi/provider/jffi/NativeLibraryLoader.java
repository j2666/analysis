/*
 * Copyright (C) 2012 Wayne Meissner
 *
 * This file is part of the JNR project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jnr.ffi.provider.jffi;

import jnr.ffi.LibraryOption;

import java.util.Collection;
import java.util.Map;

import static jnr.ffi.provider.jffi.Util.getBooleanProperty;

/**
 *
 */
class NativeLibraryLoader<T>  extends jnr.ffi.LibraryLoader<T> {
    static final boolean ASM_ENABLED = getBooleanProperty("jnr.ffi.asm.enabled", true);

    NativeLibraryLoader(Class<T> interfaceClass) {
        super(interfaceClass);
    }

    /**
     *   実際のライブラリをロードする処理
     * @param interfaceClass 呼び出したい関数が定義されているIF
     * @param libraryNames   ライブラリの名前やパスのリスト
     * @param searchPaths    ライブ理の検索パス？ The paths to search for libraries to be loaded. 
     * @param options        引数の型の対応や、関数名のリストなどが入っている。 loadメソッド内で初期化して渡している
     * @param failImmediately whether to fast-fail when the library does not implement the requested functions
     * @return an instance of {@code interfaceClass} that will call the native methods.
     */
    public T loadLibrary(Class<T> interfaceClass, 
                         Collection<String> libraryNames, 
                         Collection<String> searchPaths,
                         Map<LibraryOption, Object> options,
                         boolean failImmediately) {
        NativeLibrary nativeLibrary = new NativeLibrary(libraryNames, searchPaths, options);

        try {
            // システムプロパティ jnr.ffi.asm.enabled の値に依存して、AsmLibraryLoader または ReflectionLibraryLoaderの
            // loadLibraryを呼び出す
            return ASM_ENABLED
                ? new AsmLibraryLoader().loadLibrary(nativeLibrary, interfaceClass, options, failImmediately)
                : new ReflectionLibraryLoader().loadLibrary(nativeLibrary, interfaceClass, options, failImmediately);

        } catch (RuntimeException ex) {
            throw ex;

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
