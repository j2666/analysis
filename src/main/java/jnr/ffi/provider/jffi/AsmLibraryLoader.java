/*
 * Copyright (C) 2008-2010 Wayne Meissner
 *
 * This file is part of the JNR project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
package jnr.ffi.provider.jffi;

import com.kenai.jffi.Function;

import jnr.ffi.annotations.Variadic;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import jnr.ffi.CallingConvention;
import jnr.ffi.LibraryOption;
import jnr.ffi.annotations.Synchronized;
import jnr.ffi.mapper.CompositeTypeMapper;
import jnr.ffi.mapper.DefaultSignatureType;
import jnr.ffi.mapper.FromNativeContext;
import jnr.ffi.mapper.FunctionMapper;
import jnr.ffi.mapper.MethodResultContext;
import jnr.ffi.mapper.SignatureType;
import jnr.ffi.mapper.SignatureTypeMapper;
import jnr.ffi.provider.IdentityFunctionMapper;
import jnr.ffi.provider.InterfaceScanner;
import jnr.ffi.provider.Invoker;
import jnr.ffi.provider.NativeFunction;
import jnr.ffi.provider.NativeVariable;
import jnr.ffi.provider.ParameterType;
import jnr.ffi.provider.ResultType;
import jnr.ffi.provider.jffi.AsmBuilder.ObjectField;

import static jnr.ffi.provider.jffi.CodegenUtils.ci;
import static jnr.ffi.provider.jffi.CodegenUtils.p;
import static jnr.ffi.provider.jffi.CodegenUtils.sig;
import static jnr.ffi.provider.jffi.InvokerUtil.getCallContext;
import static jnr.ffi.provider.jffi.InvokerUtil.getCallingConvention;
import static jnr.ffi.provider.jffi.InvokerUtil.getParameterTypes;
import static jnr.ffi.provider.jffi.InvokerUtil.getResultType;
import static jnr.ffi.util.Annotations.sortedAnnotationCollection;
import static org.objectweb.asm.Opcodes.ACC_FINAL;
import static org.objectweb.asm.Opcodes.ACC_PRIVATE;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.V1_8;

/**
 * 
 */
public class AsmLibraryLoader extends LibraryLoader {
    public final static boolean DEBUG = Boolean.getBoolean("jnr.ffi.compile.dump");
    private static final AtomicLong nextClassID = new AtomicLong(0);
    private static final AtomicLong uniqueId = new AtomicLong(0);

    private static final ThreadLocal<AsmClassLoader> classLoader = new ThreadLocal<AsmClassLoader>();

    private final NativeRuntime runtime = NativeRuntime.getInstance();

    /**
     * NativeLibraryLoaderのloadLibraryから呼ばれる
     */
    @Override
    <T> T loadLibrary(NativeLibrary library, Class<T> interfaceClass, Map<LibraryOption, ?> libraryOptions,
            boolean failImmediately /* ignored, asm loader eagerly binds everything */) {
        AsmClassLoader oldClassLoader = classLoader.get();

        // Only create a new class loader if this was not a recursive call (i.e. loading
        // a library as a result of loading another library)
        // ここの理解はまだできてない
        if (oldClassLoader == null) {
            classLoader.set(new AsmClassLoader(interfaceClass.getClassLoader()));
        }
        try {
            return generateInterfaceImpl(library, interfaceClass, libraryOptions, classLoader.get());
        } finally {
            if (oldClassLoader == null)
                classLoader.remove();
        }
    }

    /**
     * linterfaceClassが実際の関数呼び出しと紐づけられたものを作って返却していそう。
     * 
     * @param <T>
     * @param library        利用する実際の so や dllと紐づけられたライブラリ
     * @param interfaceClass 利用したい関数を定義しているインタフェースクラス
     * @param libraryOptions
     * @param classLoader
     * @return
     */
    private <T> T generateInterfaceImpl(final NativeLibrary library, Class<T> interfaceClass,
            Map<LibraryOption, ?> libraryOptions,
            AsmClassLoader classLoader) {

        boolean debug = DEBUG && !interfaceClass.isAnnotationPresent(NoTrace.class);
        // ASMのClassWriterをスタックサイズなどの計算を自動で行われる設定で作成する
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        // ClassVistor は、メソッドを変更するためのIFを提供しているクラス
        ClassVisitor cv = debug ? AsmUtil.newCheckClassAdapter(cw) : cw;

        // 後で読む
        // 第1引数：runtime       引数で受け取ったruntime
        // 第2引数：classNamePath インタフェースクラスタのパス＋"$jnr$ffi$"＋シーケンス番号
        // 第3引数：classVisitor  ASMのClassvisitor
        // 第4引数：classLoader   引数で受け取ったclassLoader
        AsmBuilder builder = new AsmBuilder(runtime, p(interfaceClass) + "$jnr$ffi$" + nextClassID.getAndIncrement(),
                cv, classLoader);

        // 下記のパラメータでclassのヘッダを変更する？
        cv.visit(
                V1_8, // version ASMOpcodesで定義された定数 java1.8に対応？
                ACC_PUBLIC | ACC_FINAL, // Classをパブリック、Finalで生成
                builder.getClassNamePath(), // インタフェースクラスタのパス＋"$jnr$ffi$"＋シーケンス番号
                null, // Classのsignager
                p(AbstractAsmLibraryInterface.class), // super Class の名前
                new String[] { p(interfaceClass) } // Classのインタフェースの内部名
        );

        // functionMapperを引数で渡されたlibraryOptionsから取得する
        // ない場合は、IdentityFunctionMapper.getInstanceを設定
        // IdentityFunctionMapperは、mapFunctionNameに指定された関数名をそのまま返却するクラス
        FunctionMapper functionMapper = libraryOptions.containsKey(LibraryOption.FunctionMapper)
                ? (FunctionMapper) libraryOptions.get(LibraryOption.FunctionMapper)
                : IdentityFunctionMapper.getInstance();

        // Javaの型から、NativeType型へのMapperを取得する
        SignatureTypeMapper typeMapper = getSignatureTypeMapper(libraryOptions);
        // 取得したtypeMapperを変更されないようにラッピングする
        CompositeTypeMapper closureTypeMapper = newClosureTypeMapper(classLoader, typeMapper);

        // さらに生成したものを newCompositeTypeMapper でラッピングする
        typeMapper = newCompositeTypeMapper(runtime, classLoader, typeMapper, closureTypeMapper);

        // メソッドの呼び出しのお作法を取得する
        // https://docs.oracle.com/javase/jp/1.5.0/guide/jni/spec/design.html
        //  UNIX システムは C 呼び出し規則を使用するのに対して、
        //  Win32 システムは __stdcall を使用
        CallingConvention libraryCallingConvention = getCallingConvention(interfaceClass, libraryOptions);

        // CPU、OSをもとに対応するStubCompilerを生成する。
        // 条件
        //  1. CPUが、I386,X86_64,AARCH64のいずれかでOSがWindowsではない場合
        //     X86_32StubCompiler X86_64StubCompiler ARM_64StubCompiler
        //  2. 上記以外の場合はDummyStubCompiler
        StubCompiler compiler = StubCompiler.newCompiler(runtime);

        // 中身は使い分けされている箇所と突き合わせて読む
        // ここの詳細は読まないことにした(5/6)
        final MethodGenerator[] generators = {
                !interfaceClass.isAnnotationPresent(NoX86.class)
                        ? new X86MethodGenerator(compiler)
                        : new NotImplMethodGenerator(),
                new FastIntMethodGenerator(),
                new FastLongMethodGenerator(),
                new FastNumericMethodGenerator(),
                new BufferMethodGenerator()
        };

        DefaultInvokerFactory invokerFactory = new DefaultInvokerFactory(runtime, library, typeMapper, functionMapper,
                libraryCallingConvention, libraryOptions, interfaceClass.isAnnotationPresent(Synchronized.class));
        // 
        InterfaceScanner scanner = new InterfaceScanner(interfaceClass, typeMapper, libraryCallingConvention);

        // Intaferaceクラスのメソッドの数分
        for (NativeFunction function : scanner.functions()) {
            Method method = function.getMethod();

            // 可変長の引数を持っている
            // または
            // アノテーションが付与されている
            if (method.isVarArgs() || method.isAnnotationPresent(Variadic.class)) {
                ObjectField field = builder.getObjectField(invokerFactory.createInvoker(method), Invoker.class);
                // 内容確認する
                generateVarargsInvocation(builder, method, field);
                continue;
            }

            // 可変長引数ではない場合
            // 関数名を取得する
            String functionName = functionMapper.mapFunctionName(function.name(),
                    new NativeFunctionMapperContext(library, function.annotations()));

            try {
                // 該当する関数名のアドレスを取得する？
                long functionAddress = library.findSymbolAddress(functionName);

                // この時点では引数を内部変数にセットしているだけ
                FromNativeContext resultContext = new MethodResultContext(runtime, method);
                // 
                SignatureType signatureType = DefaultSignatureType.create(method.getReturnType(), resultContext);
                // 戻り値の型
                ResultType resultType = getResultType(
                        runtime,
                        method.getReturnType(),
                        resultContext.getAnnotations(),
                        typeMapper.getFromNativeType(signatureType, resultContext),
                        resultContext);

                // 引数用の型
                // 5/16に此処を深堀する
                // runtime : 引数でもらったNativeRuntime
                // typeMapper : 前処理で作ったCompositTypeMapper
                //                              [0] SingatureTypeMappaerAdapter
                //                              [1] InvokerTypeMapper
                //                              [2] AnnotationTypeMapper
                // 以下のメソッドは、static importで指定されている Invoker.Util.getParameterTypes
                ParameterType[] parameterTypes = getParameterTypes(runtime, typeMapper, method);

                boolean saveError = jnr.ffi.LibraryLoader.saveError(libraryOptions, function.hasSaveError(),
                        function.hasIgnoreError());

                //com.kenai.jffi.Function 
                Function jffiFunction = new Function(functionAddress,
                        getCallContext(resultType, parameterTypes, function.convention(), saveError));

                // MethodGeneratorでMethodを生成
                for (MethodGenerator g : generators) {
                    if (g.isSupported(resultType, parameterTypes, function.convention())) {
                        g.generate(builder, method.getName(), jffiFunction, resultType, parameterTypes, !saveError);
                        break;
                    }
                }

            } catch (SymbolNotFoundError ex) {
                String errorFieldName = "error_" + uniqueId.incrementAndGet();
                cv.visitField(ACC_PRIVATE | ACC_FINAL | ACC_STATIC, errorFieldName, ci(String.class), null,
                        ex.getMessage());
                generateFunctionNotFound(cv, builder.getClassNamePath(), errorFieldName, functionName,
                        method.getReturnType(), method.getParameterTypes());
            }
        }


        // 可変長メソッド用の処理？
        // generate global variable accessors
        VariableAccessorGenerator variableAccessorGenerator = new VariableAccessorGenerator(runtime);

        //  Intafaceクラスで定義されているメソッド数分 メソッドを生成
        for (NativeVariable v : scanner.variables()) {
            Method m = v.getMethod();
            // メソッドの仮の戻り値の型を表すTypeオブジェクトを取得する
            java.lang.reflect.Type variableType = ((ParameterizedType) m.getGenericReturnType())
                    .getActualTypeArguments()[0];
            if (!(variableType instanceof Class)) {
                throw new IllegalArgumentException("unsupported variable class: " + variableType);
            }
            String functionName = functionMapper.mapFunctionName(m.getName(), null);
            try {
                //対応するIFのインスタンスを生成する
                //第1引数. builder,
	            //第2引数. interfaceClass,
                //第3引数. m.getName(),
                //第4引数. library.findSymbolAddress(functionName),
                //第5引数. (Class) variableType,
                //第6引数. sortedAnnotationCollection(m.getAnnotations()),
                //第7引数. typeMapper,
                //第8引数. classLoader
                variableAccessorGenerator.generate(builder, interfaceClass, m.getName(),
                        library.findSymbolAddress(functionName), (Class) variableType,
                        sortedAnnotationCollection(m.getAnnotations()),
                        typeMapper, classLoader);

            } catch (SymbolNotFoundError ex) {
                String errorFieldName = "error_" + uniqueId.incrementAndGet();
                cv.visitField(ACC_PRIVATE | ACC_FINAL | ACC_STATIC, errorFieldName, ci(String.class), null,
                        ex.getMessage());
                generateFunctionNotFound(cv, builder.getClassNamePath(), errorFieldName, functionName,
                        m.getReturnType(), m.getParameterTypes());
            }
        }

        // コンストラクタを生成
        // Create the constructor to set the instance fields
        SkinnyMethodAdapter init = new SkinnyMethodAdapter(cv, ACC_PUBLIC, "<init>",
                sig(void.class, jnr.ffi.Runtime.class, NativeLibrary.class, Object[].class),
                null, null);
        // コンストラクタのメソッド定義開始
        init.start();
        // Invoke the super class constructor as super(Library)
        init.aload(0);
        init.aload(1);
        init.aload(2);
        init.invokespecial(p(AbstractAsmLibraryInterface.class), "<init>",
                sig(void.class, jnr.ffi.Runtime.class, NativeLibrary.class));

        builder.emitFieldInitialization(init, 3);

        init.voidreturn();
        init.visitMaxs(10, 10);
        // コンストラクタのメソッド定義終了
        init.visitEnd();

        cv.visitEnd();

        try {
            // 生成したクラスのバイトデータを取得
            byte[] bytes = cw.toByteArray();
            if (debug) {
                ClassVisitor trace = AsmUtil.newTraceClassVisitor(new PrintWriter(System.err));
                new ClassReader(bytes).accept(trace, 0);
            }

            // ClassLoaderに生成したクラスのバイトデータを渡して
            // 実際に使えるようにする。
            Class<T> implClass = classLoader.defineClass(builder.getClassNamePath().replace("/", "."), bytes);
            // コンストラクタを取得し、インスタンスを生成する
            Constructor<T> cons = implClass.getDeclaredConstructor(jnr.ffi.Runtime.class, NativeLibrary.class,
                    Object[].class);
            T result = cons.newInstance(runtime, library, builder.getObjectFieldValues());

            // Attach any native method stubs - we have to delay this until the
            // implementation class is loaded for it to work.
            System.err.flush();
            System.out.flush();

            //IFの実装クラスにライブラリの呼び出し処理を紐づけ
            compiler.attach(implClass);

            return result;
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
    }

    private void generateFunctionNotFound(ClassVisitor cv, String className, String errorFieldName, String functionName,
            Class returnType, Class[] parameterTypes) {
        SkinnyMethodAdapter mv = new SkinnyMethodAdapter(cv, ACC_PUBLIC | ACC_FINAL, functionName,
                sig(returnType, parameterTypes), null, null);
        mv.start();
        mv.getstatic(className, errorFieldName, ci(String.class));
        mv.invokestatic(AsmRuntime.class, "newUnsatisifiedLinkError", UnsatisfiedLinkError.class, String.class);
        mv.athrow();
        mv.visitMaxs(10, 10);
        mv.visitEnd();
    }

    /**
     * 可変長のパラメータを持つメソッドを生成する
     * @param builder   メソッドのバイトコードを生成するための、ClassVisitorの取得元
     * @param m         生成対象のメソッド
     * @param filed     
     */
    private void generateVarargsInvocation(AsmBuilder builder, Method m, ObjectField field) {
        Class[] parameterTypes = m.getParameterTypes();
        SkinnyMethodAdapter mv = new SkinnyMethodAdapter(builder.getClassVisitor(), ACC_PUBLIC | ACC_FINAL,
                m.getName(),
                sig(m.getReturnType(), parameterTypes), null, null);
        mv.start();

        // Retrieve the invoker
        mv.aload(0);
        mv.getfield(builder.getClassNamePath(), field.name, ci(Invoker.class));

        // Push ref to this
        mv.aload(0);

        // Construct the params array
        mv.pushInt(parameterTypes.length);
        mv.anewarray(p(Object.class));

        // パラメータを設定？
        int slot = 1;
        for (int i = 0; i < parameterTypes.length; i++) {
            mv.dup();
            mv.pushInt(i);
            if (parameterTypes[i].equals(long.class)) {
                mv.lload(slot);
                mv.invokestatic(Long.class, "valueOf", Long.class, long.class);
                slot++;
            } else if (parameterTypes[i].equals(double.class)) {
                mv.dload(slot);
                mv.invokestatic(Double.class, "valueOf", Double.class, double.class);
                slot++;
            } else if (parameterTypes[i].equals(int.class)) {
                mv.iload(slot);
                mv.invokestatic(Integer.class, "valueOf", Integer.class, int.class);
            } else if (parameterTypes[i].equals(float.class)) {
                mv.fload(slot);
                mv.invokestatic(Float.class, "valueOf", Float.class, float.class);
            } else if (parameterTypes[i].equals(short.class)) {
                mv.iload(slot);
                mv.i2s();
                mv.invokestatic(Short.class, "valueOf", Short.class, short.class);
            } else if (parameterTypes[i].equals(char.class)) {
                mv.iload(slot);
                mv.i2c();
                mv.invokestatic(Character.class, "valueOf", Character.class, char.class);
            } else if (parameterTypes[i].equals(byte.class)) {
                mv.iload(slot);
                mv.i2b();
                mv.invokestatic(Byte.class, "valueOf", Byte.class, byte.class);
            } else if (parameterTypes[i].equals(char.class)) {
                mv.iload(slot);
                mv.i2b();
                mv.invokestatic(Boolean.class, "valueOf", Boolean.class, boolean.class);
            } else {
                mv.aload(slot);
            }
            mv.aastore();
            slot++;
        }

        // call invoker(this, parameters)
        mv.invokeinterface(jnr.ffi.provider.Invoker.class, "invoke", Object.class, Object.class, Object[].class);

        Class<?> returnType = m.getReturnType();
        if (returnType.equals(long.class)) {
            mv.checkcast(Long.class);
            mv.invokevirtual(Long.class, "longValue", long.class);
            mv.lreturn();
        } else if (returnType.equals(double.class)) {
            mv.checkcast(Double.class);
            mv.invokevirtual(Double.class, "doubleValue", double.class);
            mv.dreturn();
        } else if (returnType.equals(int.class)) {
            mv.checkcast(Integer.class);
            mv.invokevirtual(Integer.class, "intValue", int.class);
            mv.ireturn();
        } else if (returnType.equals(float.class)) {
            mv.checkcast(Float.class);
            mv.invokevirtual(Float.class, "floatValue", float.class);
            mv.freturn();
        } else if (returnType.equals(short.class)) {
            mv.checkcast(Short.class);
            mv.invokevirtual(Short.class, "shortValue", short.class);
            mv.ireturn();
        } else if (returnType.equals(char.class)) {
            mv.checkcast(Character.class);
            mv.invokevirtual(Character.class, "charValue", char.class);
            mv.ireturn();
        } else if (returnType.equals(byte.class)) {
            mv.checkcast(Byte.class);
            mv.invokevirtual(Byte.class, "byteValue", byte.class);
            mv.ireturn();
        } else if (returnType.equals(boolean.class)) {
            mv.checkcast(Boolean.class);
            mv.invokevirtual(Boolean.class, "booleanValue", boolean.class);
            mv.ireturn();
        } else if (void.class.isAssignableFrom(m.getReturnType())) {
            mv.voidreturn();
        } else {
            mv.checkcast(m.getReturnType());
            mv.areturn();
        }

        mv.visitMaxs(100, AsmUtil.calculateLocalVariableSpace(parameterTypes) + 1);
        // メソッドの定義終了
        mv.visitEnd();
    }
}
