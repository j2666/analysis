/*
 * Copyright (C) 2008-2010 Wayne Meissner
 *
 * This file is part of the JNR project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jnr.ffi.provider.jffi;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class AsmClassLoader extends ClassLoader {
    private final ConcurrentMap<String, Class> definedClasses = new ConcurrentHashMap<String, Class>();

    public AsmClassLoader() {
    }

    public AsmClassLoader(ClassLoader parent) {
        super(parent);
    }


    public Class defineClass(String name, byte[] b) {
        // バイト配列をもとに、指定した名前のクラスのインスタンスに変換し
        // https://docs.oracle.com/javase/jp/7/api/java/lang/ClassLoader.html
        Class klass = defineClass(name, b, 0, b.length);

        //内部のマップに追加
        definedClasses.putIfAbsent(name, klass);
        
        // 生成したクラスを利用できるようにリンクする。
        // resoleveClassを呼び出すことで、
        // このクラスを作成したClassLoaderクラスの中の
        // loadClass()というメソッドを繰り返し使って、解決されていない
        // 参照クラスを解決しようとします。
        //   （引用元）https://www.ikueikan.ac.jp/biblion/1996/JavaAppli/node44.html
        resolveClass(klass);
        return klass;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class klass = definedClasses.get(name);
        if (klass != null) {
            return klass;
        }
        return super.findClass(name);
    }
}
