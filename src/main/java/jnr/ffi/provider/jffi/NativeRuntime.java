/*
 * Copyright (C) 2008-2010 Wayne Meissner
 *
 * This file is part of the JNR project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jnr.ffi.provider.jffi;

import jnr.ffi.NativeType;
import jnr.ffi.ObjectReferenceManager;
import jnr.ffi.Platform;
import jnr.ffi.Runtime;
import jnr.ffi.Type;
import jnr.ffi.TypeAlias;
import jnr.ffi.mapper.DefaultTypeMapper;
import jnr.ffi.mapper.SignatureTypeMapperAdapter;
import jnr.ffi.provider.AbstractRuntime;
import jnr.ffi.provider.BadType;
import jnr.ffi.provider.DefaultObjectReferenceManager;

import java.lang.reflect.Field;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public final class NativeRuntime extends AbstractRuntime {
    private final NativeMemoryManager mm = new NativeMemoryManager(this);
    private final NativeClosureManager closureManager = new NativeClosureManager(this,
            new SignatureTypeMapperAdapter(new DefaultTypeMapper()));

    /**
     * jnr.ffi.TypeAliasesで定義してある型名に対応するNativeTypeを保持する
     */
    private final Type[] aliases;

    // WeakHashMap to auto remove GC'd NativeLibraries
    final WeakHashMap<NativeLibrary, NativeLibrary.LoadedLibraryData> loadedLibraries = new WeakHashMap<>();

    public static NativeRuntime getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * See and use {@link Runtime#getLoadedLibraries()} (which forwards here)
     * instead of this directly
     */
    public static List<NativeLibrary.LoadedLibraryData> getLoadedLibraries() {
        if (getSystemRuntime() instanceof NativeRuntime) {
            return new ArrayList<>(((NativeRuntime) getSystemRuntime()).loadedLibraries.values());
        } else
            return Collections.emptyList();
    }

    private static final class SingletonHolder {
        public static final NativeRuntime INSTANCE = new NativeRuntime();
    }

    private NativeRuntime() {
        // AbstractRuntimeのコンストラクタに
        // java.nio.ByteOrder.nativeOrderで、エンディアンを指定
        // buildTypeMapによって、NativeTypeに対応するcom.kenai.jffi.Typeで定義されている型、サイズ、Allingmentの対応がわかるマップを作成して
        // コンストラクタにわたすと、親クラス（AbstractRuntime）内に渡して初期化
        super(ByteOrder.nativeOrder(), buildTypeMap());

        // jnr.ffi.TypeAliasesで定義してある順序でロードした環境に依存したTypeAliasクラスで記載されているNativeTypeを保持する。
        NativeType[] nativeAliases = buildNativeTypeAliases();
        
        // 取得してきたnativeAliasesをもとにメンバ変数aliasesを初期化

        EnumSet<TypeAlias> typeAliasSet = EnumSet.allOf(TypeAlias.class);
        aliases = new Type[typeAliasSet.size()];

        // Aliasごとに、
        for (TypeAlias alias : typeAliasSet) {
            // ordinal →　列挙定数の序数
            // 参照先である、nativeAliasesのサイズ内であること。
            // かつ、
            // nativeAliasesで、今参照しようとしているaliasの位置で設定されているnativeTypeの型がVoidでないことをチェック
            if (nativeAliases.length > alias.ordinal() && nativeAliases[alias.ordinal()] != NativeType.VOID) {
                // メンバ変数 aliasesに、nativeAliasesで設定された型を設定する。
                //　findTypeメソッドを呼び出しているが、現時点では単に対応する位置のものを返却しているだけ。
                aliases[alias.ordinal()] = findType(nativeAliases[alias.ordinal()]);
            } else {
            // aliasにNativeTypeが未定義の際は、"BadType"を設定する。
            // なぜなら、buildNativeTypeAliasesの中で一致するように実装しているため。
                aliases[alias.ordinal()] = new BadType(alias.name());
            }
        }
    }

    /**
     * NativeTypeに対応するcom.kenai.jffi.Typeで定義されている型、サイズ。Allingmentの対応がわかるマップを作成する
     * 実際には、jnr.ffi.Type で隠蔽されてる。
     * コンストラクタに渡すためのbuildTypeMapを初期化するために使われている
     * 
     * @return
     */
    private static EnumMap<NativeType, Type> buildTypeMap() {

        // typeMap変数を、keyの型にjnr.ffi.NativeTypeで初期化
        EnumMap<NativeType, Type> typeMap = new EnumMap<NativeType, Type>(NativeType.class);

        // jrn
        EnumSet<NativeType> nativeTypes = EnumSet.allOf(NativeType.class);

        // natievTypeの要素に対応する、com.kenai.jffi.Typeや、サイズ等がわかるようマップを作る。
        for (NativeType t : nativeTypes) {
            typeMap.put(t, jafflType(t));
        }

        return typeMap;
    }

    /**
     * Platformに応じてtime_tなどが実際のNativeTypeでいうとどの型に対応するかがわかる配列を返却する
     * 配列の順番は jnr.ffi.TypeAliasesで定義してある順序を保障する
     * 1. 実際の型の対応は以下のクラス定義されているものが動的に読み込まれる
     *     jnr.ffi.provider.jffi.platform.CPU種別.OS種別.TypeAliase
     *         TypeAlias.int8_t, NativeType.SCHAR
     *           :
     *         TypeAlias.time_t , NativeType.SLONG
     * 
     * 2. 返却する配列の順序は、jnr.ffi.TypeAliasesdで以下のように定義されている
     *   
     *         public enum TypeAlias{ 
     *             int8_t,
     *             u_int8_t,
     *              :
     *             time_t, 
     *              :
     * 
     * 返却するNativeType[]は、x86_64, Linux環境の場合
     * NativeType[0]は、int8_tに対応するNativeType.SCHAR
     * NativeType[31]が、time_tに対応するNativeType.SLONG
     * となっている
     * @return
     */
    private static NativeType[] buildNativeTypeAliases() {
        //この先でOSや、アーキテクチャの判断もして対応するPlatformの変数を返す。
        Platform platform = Platform.getNativePlatform();

        // jnr.ffi.provider.jffi
        Package pkg = NativeRuntime.class.getPackage();
        String cpu = platform.getCPU().toString();
        String os = platform.getOS().toString();

        // Typeで定義している、データの大きさに対応するための基本的な型にたいして
        // time_tなど、基本的な型をもとに定義できる型の定義をここで持つ
        // platfromに
        EnumSet<TypeAlias> typeAliases = EnumSet.allOf(TypeAlias.class);
        NativeType[] aliases = {};
        Class cls;
        try {
            // jnr.ffi.provider.jffi.platform.実行環境のCPUアーキテクチャ.実行環境のOS.TypeAliases
            // x86_64でLinuxの場合は、
            /// jnr.ffi.provider.jffi.platform.x86_64.linux.TypeAliasesがロードされる
            cls = Class.forName(pkg.getName() + ".platform." + cpu + "." + os + ".TypeAliases");

            // jnr.ffi.TypeAliasesで定義してある順序で
            // ロードした環境に依存したTypeAliasクラスで記載されているNativeTypeとの対応を配列化
            // 
            // 作成した配列を返却する

            Field aliasesField = cls.getField("ALIASES");
            Map aliasMap = Map.class.cast(aliasesField.get(cls));
            aliases = new NativeType[typeAliases.size()];
            for (TypeAlias t : typeAliases) {
                aliases[t.ordinal()] = (NativeType) aliasMap.get(t);
                if (aliases[t.ordinal()] == null) {
                    aliases[t.ordinal()] = NativeType.VOID;
                }
            }
        } catch (ClassNotFoundException cne) {
            Logger.getLogger(NativeRuntime.class.getName()).log(Level.SEVERE, "failed to load type aliases: " + cne);
        } catch (NoSuchFieldException nsfe) {
            Logger.getLogger(NativeRuntime.class.getName()).log(Level.SEVERE, "failed to load type aliases: " + nsfe);
        } catch (IllegalAccessException iae) {
            Logger.getLogger(NativeRuntime.class.getName()).log(Level.SEVERE, "failed to load type aliases: " + iae);
        }

        return aliases;
    }

    @Override
    public Type findType(TypeAlias type) {
        return aliases[type.ordinal()];
    }

    public final NativeMemoryManager getMemoryManager() {
        return mm;
    }

    public NativeClosureManager getClosureManager() {
        return closureManager;
    }

    @Override
    public ObjectReferenceManager newObjectReferenceManager() {
        return new DefaultObjectReferenceManager(this);
    }

    @Override
    public int getLastError() {
        return com.kenai.jffi.LastError.getInstance().get();
    }

    @Override
    public void setLastError(int error) {
        com.kenai.jffi.LastError.getInstance().set(error);
    }

    @Override
    public boolean isCompatible(Runtime other) {
        return other instanceof NativeRuntime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        NativeRuntime that = (NativeRuntime) o;

        return Arrays.equals(aliases, that.aliases) && closureManager.equals(that.closureManager) && mm.equals(that.mm);
    }

    @Override
    public int hashCode() {
        int result = mm.hashCode();
        result = 31 * result + closureManager.hashCode();
        result = 31 * result + Arrays.hashCode(aliases);
        return result;
    }

    /**
     * jnr.ffi.Typeの拡張した実態。なぜ、間に挟んでいるのかは不明
     */
    private static final class TypeDelegate extends jnr.ffi.Type {
        private final com.kenai.jffi.Type type;
        private final NativeType nativeType;

        /**
         * NativeTypeに対応するcom.kenai.jffi.Typeの対応を管理する
         * com.kenai.jffi.Typeが、
         * 
         * @param type
         * @param nativeType
         */
        public TypeDelegate(com.kenai.jffi.Type type, NativeType nativeType) {
            this.type = type;
            this.nativeType = nativeType;
        }

        /**
         * The alignment of this type, in bytes.
         */
        public int alignment() {
            return type.alignment();
        }

        /**
         * The size of this type, in bytes.
         */
        public int size() {
            return type.size();
        }

        /**
         * NativeTypeを返却する
         */
        public NativeType getNativeType() {
            return nativeType;
        }

        public String toString() {
            return type.toString();
        }
    }

    /**
     * 指定されたNativeTypeに対するTypeDelegateを生成して返却する。
     * TypeDelegateは、nativeTypeに対応するcom.kenai.jffiの型とサイズ、allignmentを返却する
     * 
     * @param type
     * @return
     */
    private static jnr.ffi.Type jafflType(NativeType type) {
        switch (type) {
            case VOID:
                return new TypeDelegate(com.kenai.jffi.Type.VOID, NativeType.VOID);
            case SCHAR:
                return new TypeDelegate(com.kenai.jffi.Type.SCHAR, NativeType.SCHAR);
            case UCHAR:
                return new TypeDelegate(com.kenai.jffi.Type.UCHAR, NativeType.UCHAR);
            case SSHORT:
                return new TypeDelegate(com.kenai.jffi.Type.SSHORT, NativeType.SSHORT);
            case USHORT:
                return new TypeDelegate(com.kenai.jffi.Type.USHORT, NativeType.USHORT);
            case SINT:
                return new TypeDelegate(com.kenai.jffi.Type.SINT, NativeType.SINT);
            case UINT:
                return new TypeDelegate(com.kenai.jffi.Type.UINT, NativeType.UINT);
            case SLONG:
                return new TypeDelegate(com.kenai.jffi.Type.SLONG, NativeType.SLONG);
            case ULONG:
                return new TypeDelegate(com.kenai.jffi.Type.ULONG, NativeType.ULONG);
            case SLONGLONG:
                return new TypeDelegate(com.kenai.jffi.Type.SINT64, NativeType.SLONGLONG);
            case ULONGLONG:
                return new TypeDelegate(com.kenai.jffi.Type.UINT64, NativeType.ULONGLONG);
            case FLOAT:
                return new TypeDelegate(com.kenai.jffi.Type.FLOAT, NativeType.FLOAT);
            case DOUBLE:
                return new TypeDelegate(com.kenai.jffi.Type.DOUBLE, NativeType.DOUBLE);
            case ADDRESS:
                return new TypeDelegate(com.kenai.jffi.Type.POINTER, NativeType.ADDRESS);
            default:
                return new BadType(type.toString());
        }
    }
}
